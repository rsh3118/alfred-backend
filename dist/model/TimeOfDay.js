"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeOfDay = void 0;
class TimeOfDay {
    constructor(timeOfDayString) {
        this.toString = () => {
            return `${this.getFormattedTimeFieldString(this.hour)}:${this.getFormattedTimeFieldString(this.hour)}`;
        };
        let timeOfDayArray = timeOfDayString.split(":");
        let timeInHours = timeOfDayArray[0];
        let timeInMinutes = timeOfDayArray[1];
        if (+timeInHours < 0 || +timeInHours > 23) {
            throw new Error(`time in hours for ${timeOfDayString} must be between 0 and 23`);
        }
        if (+timeInMinutes < 0 || +timeInMinutes > 60) {
            throw new Error(`time in minutes for ${timeOfDayString} must be between 0 and 60`);
        }
        this.hour = +timeInHours;
        this.minute = +timeInMinutes;
    }
    getFormattedTimeFieldString(timeFieldValue) {
        return '0'.repeat(Math.max(2 - timeFieldValue.toString().length, 0));
    }
}
exports.TimeOfDay = TimeOfDay;
