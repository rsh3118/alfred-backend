"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.convertToDate = exports.getMinutesLeftInADay = exports.isFriday = exports.isWeekend = exports.isWeekday = exports.isEndOfMonth = exports.isSameMonth = exports.isSameWeek = exports.isSameDay = exports.convertToGMT = exports.convertToCST = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const utc_1 = __importDefault(require("dayjs/plugin/utc"));
const timezone_1 = __importDefault(require("dayjs/plugin/timezone"));
dayjs_1.default.extend(utc_1.default);
dayjs_1.default.extend(timezone_1.default);
dayjs_1.default.tz.setDefault("America/Los_Angeles");
function convertToCST(day) {
    return day.subtract(6, 'hour');
}
exports.convertToCST = convertToCST;
function convertToGMT(day) {
    return day.add(6, 'hour');
}
exports.convertToGMT = convertToGMT;
function isSameDay(day1, day2) {
    return day1.format('MM/DD/YYYY') === day2.format('MM/DD/YYYY');
}
exports.isSameDay = isSameDay;
function isSameWeek(day1, day2) {
    return day1.startOf('week').format('MM/DD/YYYY') === day2.startOf('week').format('MM/DD/YYYY');
}
exports.isSameWeek = isSameWeek;
function isSameMonth(day1, day2) {
    return day1.format('MM/YYYY') === day2.format('MM/YYYY');
}
exports.isSameMonth = isSameMonth;
function isEndOfMonth(day1) {
    // day1 = convertToGMT(day1)
    return day1.format('MM/DD/YYYY') === day1.endOf('month').format('MM/DD/YYYY');
}
exports.isEndOfMonth = isEndOfMonth;
function isWeekday(day1) {
    return day1.day() < 5;
}
exports.isWeekday = isWeekday;
function isWeekend(day1) {
    return day1.day() > 4;
}
exports.isWeekend = isWeekend;
function isFriday(day1) {
    return day1.day() === 4;
}
exports.isFriday = isFriday;
function getMinutesLeftInADay() {
    let currentTime = (0, dayjs_1.default)();
    let hour = currentTime.hour();
    let minute = currentTime.minute();
    return 60 * (22 - hour) + (60 - minute);
}
exports.getMinutesLeftInADay = getMinutesLeftInADay;
function convertToDate(dateString, format) {
    return (0, dayjs_1.default)(dateString, format);
}
exports.convertToDate = convertToDate;
