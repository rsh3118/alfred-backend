"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
var LogColor;
(function (LogColor) {
    LogColor["RESET"] = "\u001B[0m";
    LogColor["BRIGHT"] = "\u001B[1m";
    LogColor["DIM"] = "\u001B[2m";
    LogColor["UNDERSCORE"] = "\u001B[4m";
    LogColor["BLINK"] = "\u001B[5m";
    LogColor["REVERSE"] = "\u001B[7m";
    LogColor["HIDDEN"] = "\u001B[8m";
    LogColor["FG_BLACK"] = "\u001B[30m";
    LogColor["FG_RED"] = "\u001B[31m";
    LogColor["FG_GREEN"] = "\u001B[32m";
    LogColor["FG_YELLOW"] = "\u001B[33m";
    LogColor["FG_BLUE"] = "\u001B[34m";
    LogColor["FG_MAGENTA"] = "\u001B[35m";
    LogColor["FG_CYAN"] = "\u001B[36m";
    LogColor["FG_WHITE"] = "\u001B[37m";
    LogColor["BG_BLACK"] = "\u001B[40m";
    LogColor["BG_RED"] = "\u001B[41m";
    LogColor["BG_GREEN"] = "\u001B[42m";
    LogColor["BG_YELLOW"] = "\u001B[43m";
    LogColor["BG_BLUE"] = "\u001B[44m";
    LogColor["BG_MAGENTA"] = "\u001B[45m";
    LogColor["BG_CYAN"] = "\u001B[46m";
    LogColor["BG_WHITE"] = "\u001B[47m";
})(LogColor || (LogColor = {}));
class Logger {
    constructor(identifier, enabled = true) {
        this.identifier = identifier;
        this.enabled = enabled;
    }
    info(msg, obj) {
        this.log(LogColor.FG_WHITE, msg, obj);
    }
    error(msg, obj) {
        this.log(LogColor.FG_RED, msg, obj);
    }
    log(color, msg, obj) {
        if (!this.enabled) {
            return;
        }
        if (obj instanceof Map) {
            obj = Array.from(obj.entries());
        }
        let detailedLog = obj ?
            `
${JSON.stringify(obj, null, 2)}
    `
            :
                "";
        console.log(`${color}%s${LogColor.RESET}`, `[${this.identifier}] ${msg} ${detailedLog}`);
    }
}
exports.Logger = Logger;
