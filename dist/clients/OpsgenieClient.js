"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpsgenieClient = exports.OpsgenieAction = void 0;
const Logger_1 = require("../util/Logger");
var opsgeniePhoneInstance = require('opsgenie-sdk');
var opsgenieNotificationInstance = require('opsgenie-sdk');
var OpsgenieAction;
(function (OpsgenieAction) {
    OpsgenieAction["CALL_AND_TEXT"] = "CALL AND TEXT";
    OpsgenieAction["PUSH_NOTIFICATION"] = "PUSH_NOTIFICATION";
})(OpsgenieAction = exports.OpsgenieAction || (exports.OpsgenieAction = {}));
class OpsgenieClient {
    constructor() { }
    createAlert(action, message) {
        let logger = new Logger_1.Logger("OpsgenieClient:createAlert", false);
        let alert = { message };
        let opsgenieInstance = opsgenieNotificationInstance;
        switch (action) {
            case OpsgenieAction.CALL_AND_TEXT:
                opsgeniePhoneInstance.configure({
                    'api_key': '7fb556f4-1b33-48c4-8191-ed2e024a40c7'
                });
                break;
            case OpsgenieAction.PUSH_NOTIFICATION:
                opsgenieNotificationInstance.configure({
                    'api_key': '1f3aa085-f8f8-4704-91ff-98cca4ffb879'
                });
                break;
            default:
                break;
        }
        opsgenieInstance.alertV2.create(alert, function (error, alert) {
            if (error) {
                logger.error(error);
            }
            else {
                logger.info(alert);
            }
        });
    }
}
exports.OpsgenieClient = OpsgenieClient;
