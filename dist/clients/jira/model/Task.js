"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Task = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const JiraTicket_1 = require("./JiraTicket");
const Priority_1 = require("./Priority");
const utc_1 = __importDefault(require("dayjs/plugin/utc"));
const timezone_1 = __importDefault(require("dayjs/plugin/timezone"));
dayjs_1.default.extend(utc_1.default);
dayjs_1.default.extend(timezone_1.default);
dayjs_1.default.tz.setDefault("America/Los_Angeles");
class Task extends JiraTicket_1.JiraTicket {
    constructor(apiResponseIssue, keyObject) {
        super(apiResponseIssue, keyObject);
        this.alarmStartTimeInMinutes = this.fieldsMap.get("Daily Start Time");
        this.alarmEndTimeInMinutes = this.fieldsMap.get("Daily End Time");
        this.storyPoints = this.fieldsMap.get("Story point estimate") || 0;
        if (this.fieldsMap.get("Priority")) {
            this.priority = this.fieldsMap.get("Priority").name;
        }
        else {
            this.priority = Priority_1.Priority.LOWEST;
        }
        if (apiResponseIssue.fields.duedate) {
            this.dueDate = dayjs_1.default.tz(apiResponseIssue.fields.duedate);
            //this.dueDate = convertToGMT(this.dueDate)
        }
        else {
            this.dueDate = (0, dayjs_1.default)(new Date(10000));
        }
    }
}
exports.Task = Task;
