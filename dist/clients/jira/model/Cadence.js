"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EverydayExceptFridayCadence = exports.WeekendCadence = exports.WeekdayCadence = exports.LastDayOfTheMonthCadence = exports.DayOfTheMonthCadence = exports.DayOfMonth = exports.DayOfWeekCadence = exports.DayOfWeek = exports.EveryXDaysCadence = exports.MonthlyCadence = exports.WeeklyCadence = exports.DailyCadence = exports.Cadence = exports.createCadence = void 0;
function getCapturedGroup(arr) {
    if (arr === null) {
        throw new Error("regex not matched");
    }
    let group = null;
    arr.forEach(function a(value, index, array) {
        if (index === 1) {
            group = value;
        }
    });
    if (group === null) {
        throw new Error("couldn't find capture group");
    }
    return group;
}
function createCadence(str) {
    let cadenceValue = str.toLowerCase();
    if (cadenceValue.match(DAILY_CADENCE_REGEX_MATCHER)) {
        return new DailyCadence();
    }
    if (cadenceValue.match(WEEKLY_CADENCE_REGEX_MATCHER)) {
        return new WeeklyCadence();
    }
    if (cadenceValue.match(MONTHLY_CADENCE_REGEX_MATCHER)) {
        return new MonthlyCadence();
    }
    if (cadenceValue.match(EVERYXDAY_CADENCE_REGEX_MATCHER)) {
        let group = getCapturedGroup(cadenceValue.match(EVERYXDAY_CADENCE_REGEX_MATCHER));
        return new EveryXDaysCadence(Number(group));
    }
    if (cadenceValue.match(DAY_OF_THE_WEEK_REGEX_MATCHER)) {
        let group = getCapturedGroup(cadenceValue.match(DAY_OF_THE_WEEK_REGEX_MATCHER));
        return new DayOfWeekCadence(group);
    }
    if (cadenceValue.match(DAY_OF_THE_MONTH_CADENCE_REGEX_MATCHER)) {
        let group = getCapturedGroup(cadenceValue.match(DAY_OF_THE_MONTH_CADENCE_REGEX_MATCHER));
        return new DayOfTheMonthCadence(group);
    }
    if (cadenceValue.match(LAST_DAY_OF_THE_MONTH_CADENCE_REGEX_MATCH)) {
        return new LastDayOfTheMonthCadence();
    }
    if (cadenceValue.match(EVERY_WEEKDAY_CADENCE_REGEX_MATCHER)) {
        return new WeekdayCadence();
    }
    if (cadenceValue.match(EVERY_WEEKEND_DAY_CADENCE_REGEX_MATCHER)) {
        return new WeekendCadence();
    }
    if (cadenceValue.match(EVERYDAY_EXCEPT_FRIDAY_CADENCE)) {
        return new EverydayExceptFridayCadence();
    }
    throw new Error("didn't match any cadence");
}
exports.createCadence = createCadence;
class Cadence {
}
exports.Cadence = Cadence;
let DAILY_CADENCE_REGEX_MATCHER = /daily/;
class DailyCadence extends Cadence {
    constructor() {
        super(...arguments);
        this.toString = () => {
            return `Daily Cadence`;
        };
    }
}
exports.DailyCadence = DailyCadence;
let WEEKLY_CADENCE_REGEX_MATCHER = /weekly/;
class WeeklyCadence extends Cadence {
    constructor() {
        super(...arguments);
        this.toString = () => {
            return `Weekly Cadence`;
        };
    }
}
exports.WeeklyCadence = WeeklyCadence;
let MONTHLY_CADENCE_REGEX_MATCHER = /monthly/;
class MonthlyCadence extends Cadence {
    constructor() {
        super(...arguments);
        this.toString = () => {
            return `Monthly Cadence`;
        };
    }
}
exports.MonthlyCadence = MonthlyCadence;
let EVERYXDAY_CADENCE_REGEX_MATCHER = /every\s([\d]+)\sdays/;
class EveryXDaysCadence extends Cadence {
    constructor(x) {
        super();
        this.toString = () => {
            return `Every ${this.x} Days Cadence`;
        };
        this.x = x;
    }
}
exports.EveryXDaysCadence = EveryXDaysCadence;
let DAY_OF_THE_WEEK_REGEX_MATCHER = /every\s(monday|tuesday|wednesday|thursday|friday|saturday|sunday)/;
var DayOfWeek;
(function (DayOfWeek) {
    DayOfWeek["MONDAY"] = "moday";
    DayOfWeek["TUESDAY"] = "tuesday";
    DayOfWeek["WEDNESDAY"] = "wednesday";
    DayOfWeek["THURSDAY"] = "thursday";
    DayOfWeek["FRIDAY"] = "friday";
    DayOfWeek["SATURDAY"] = "saturday";
    DayOfWeek["SUNDAY"] = "sunday";
})(DayOfWeek = exports.DayOfWeek || (exports.DayOfWeek = {}));
class DayOfWeekCadence extends Cadence {
    constructor(dayOfWeek) {
        super();
        this.toString = () => {
            return `Every ${this.dayOfWeek} Cadence`;
        };
        this.dayOfWeek = dayOfWeek;
    }
    getDayOfWeekNumber() {
        if (this.dayOfWeek == DayOfWeek.MONDAY) {
            return 0;
        }
        if (this.dayOfWeek == DayOfWeek.TUESDAY) {
            return 1;
        }
        if (this.dayOfWeek == DayOfWeek.WEDNESDAY) {
            return 2;
        }
        if (this.dayOfWeek == DayOfWeek.THURSDAY) {
            return 3;
        }
        if (this.dayOfWeek == DayOfWeek.FRIDAY) {
            return 4;
        }
        if (this.dayOfWeek == DayOfWeek.SATURDAY) {
            return 5;
        }
        if (this.dayOfWeek == DayOfWeek.SUNDAY) {
            return 6;
        }
        throw new Error("Could not map day of week");
    }
}
exports.DayOfWeekCadence = DayOfWeekCadence;
let DAY_OF_THE_MONTH_CADENCE_REGEX_MATCHER = /([\d]+)[\w]*\sof\sthe\smonth/;
var DayOfMonth;
(function (DayOfMonth) {
    DayOfMonth["DAY_OF_MONTH_1"] = "1";
    DayOfMonth["DAY_OF_MONTH_2"] = "2";
    DayOfMonth["DAY_OF_MONTH_3"] = "3";
    DayOfMonth["DAY_OF_MONTH_4"] = "4";
    DayOfMonth["DAY_OF_MONTH_5"] = "5";
    DayOfMonth["DAY_OF_MONTH_6"] = "6";
    DayOfMonth["DAY_OF_MONTH_7"] = "7";
    DayOfMonth["DAY_OF_MONTH_8"] = "8";
    DayOfMonth["DAY_OF_MONTH_9"] = "9";
    DayOfMonth["DAY_OF_MONTH_10"] = "10";
    DayOfMonth["DAY_OF_MONTH_11"] = "11";
    DayOfMonth["DAY_OF_MONTH_12"] = "12";
    DayOfMonth["DAY_OF_MONTH_13"] = "13";
    DayOfMonth["DAY_OF_MONTH_14"] = "14";
    DayOfMonth["DAY_OF_MONTH_15"] = "15";
    DayOfMonth["DAY_OF_MONTH_16"] = "16";
    DayOfMonth["DAY_OF_MONTH_17"] = "17";
    DayOfMonth["DAY_OF_MONTH_18"] = "18";
    DayOfMonth["DAY_OF_MONTH_19"] = "19";
    DayOfMonth["DAY_OF_MONTH_20"] = "20";
    DayOfMonth["DAY_OF_MONTH_21"] = "21";
    DayOfMonth["DAY_OF_MONTH_22"] = "22";
    DayOfMonth["DAY_OF_MONTH_23"] = "23";
    DayOfMonth["DAY_OF_MONTH_24"] = "24";
    DayOfMonth["DAY_OF_MONTH_25"] = "25";
    DayOfMonth["DAY_OF_MONTH_26"] = "26";
    DayOfMonth["DAY_OF_MONTH_27"] = "27";
    DayOfMonth["DAY_OF_MONTH_28"] = "28";
    DayOfMonth["DAY_OF_MONTH_29"] = "29";
    DayOfMonth["DAY_OF_MONTH_30"] = "30";
    DayOfMonth["DAY_OF_MONTH_31"] = "31";
})(DayOfMonth = exports.DayOfMonth || (exports.DayOfMonth = {}));
class DayOfTheMonthCadence extends Cadence {
    constructor(dayOfMonth) {
        super();
        this.toString = () => {
            return `Every ${this.dayOfMonth} of the Month Cadence`;
        };
        this.dayOfMonth = dayOfMonth;
    }
}
exports.DayOfTheMonthCadence = DayOfTheMonthCadence;
let LAST_DAY_OF_THE_MONTH_CADENCE_REGEX_MATCH = /last\sday\sof\sthe\smonth/;
class LastDayOfTheMonthCadence extends Cadence {
    constructor() {
        super(...arguments);
        this.toString = () => {
            return `Last Day of Every Month Cadence`;
        };
    }
}
exports.LastDayOfTheMonthCadence = LastDayOfTheMonthCadence;
let EVERY_WEEKDAY_CADENCE_REGEX_MATCHER = /every\sday\sof\sthe\sworking\sweek/;
class WeekdayCadence extends Cadence {
    constructor() {
        super(...arguments);
        this.toString = () => {
            return `Every Weekday Cadence`;
        };
    }
}
exports.WeekdayCadence = WeekdayCadence;
let EVERY_WEEKEND_DAY_CADENCE_REGEX_MATCHER = /every\sday\sof\sthe\sweekend/;
class WeekendCadence extends Cadence {
    constructor() {
        super(...arguments);
        this.toString = () => {
            return `Every Weekend Cadence`;
        };
    }
}
exports.WeekendCadence = WeekendCadence;
let EVERYDAY_EXCEPT_FRIDAY_CADENCE = /every\sday\sexcept\sfriday/;
class EverydayExceptFridayCadence extends Cadence {
    constructor() {
        super(...arguments);
        this.toString = () => {
            return `Every Day Except Friday Cadence`;
        };
    }
}
exports.EverydayExceptFridayCadence = EverydayExceptFridayCadence;
// export enum Cadence {
//     // common cadences
//     DAILY,
//     WEEKLY,
//     MONTHLY,
//     // every x days
//     EVERY_2_DAYS,
//     EVERY_3_DAYS,
//     EVERY_4_DAYS,
//     EVERY_5_DAYS,
//     EVERY_6_DAYS,
//     // only on specific days of the week
//     MONDAY,
//     TUESDAY,
//     WEDNESDAY,
//     THURSDAY,
//     FRIDAY,
//     SATURDAY,
//     SUNDAY,
//     // only on specific days of the month
//     MONTH_1,
//     MONTH_2,
//     MONTH_3,
//     MONTH_4,
//     MONTH_5,
//     MONTH_6,
//     MONTH_7,
//     MONTH_8,
//     MONTH_9,
//     MONTH_10,
//     MONTH_11,
//     MONTH_12,
//     MONTH_13,
//     MONTH_14,
//     MONTH_15,
//     MONTH_16,
//     MONTH_17,
//     MONTH_18,
//     MONTH_19,
//     MONTH_20,
//     MONTH_21,
//     MONTH_22,
//     MONTH_23,
//     MONTH_24,
//     MONTH_25,
//     MONTH_26,
//     MONTH_27,
//     MONTH_28,
//     MONTH_29,
//     MONTH_30,
//     MONTH_31
// }
