"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Status = void 0;
var Status;
(function (Status) {
    Status["TODO"] = "To Do";
    Status["DONE"] = "Done";
    Status["FAILED"] = "Failed";
})(Status = exports.Status || (exports.Status = {}));
