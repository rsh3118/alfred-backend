"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CALL_AND_TEXT = exports.PUSH_NOTIFICATION = exports.PREFIX = exports.NOTIFICATION = exports.NOTIFICATION_END_TIME = exports.NOTIFICATION_START_TIME = exports.ALERTING_METHODS = exports.SNOOZE_UNTIL = exports.HIGH_PRIORITY_ALERT_CONFIG_KEY = exports.MEDIUM_PRIORITY_ALERT_CONFIG_KEY = exports.LOW_PRIORITY_ALERT_CONFIG_KEY = exports.Sprint = exports.NEXT_WEEK_SPRINT_ID = exports.TOMORROW_SPRINT_ID = exports.TODAY_SPRINT_ID = void 0;
// Sprint Constant
exports.TODAY_SPRINT_ID = '25';
exports.TOMORROW_SPRINT_ID = '26';
exports.NEXT_WEEK_SPRINT_ID = '27';
// Sprint Enums
var Sprint;
(function (Sprint) {
    Sprint["TODAY"] = "TODAY";
    Sprint["TOMORROW"] = "TOMORROW";
    Sprint["NEXT_WEEK"] = "NEXT_WEEK";
})(Sprint = exports.Sprint || (exports.Sprint = {}));
// Alert Config Constants
exports.LOW_PRIORITY_ALERT_CONFIG_KEY = 'CONFIG-3';
exports.MEDIUM_PRIORITY_ALERT_CONFIG_KEY = 'CONFIG-5';
exports.HIGH_PRIORITY_ALERT_CONFIG_KEY = 'CONFIG-4';
// field names
exports.SNOOZE_UNTIL = "Snooze Until";
exports.ALERTING_METHODS = "Alerting Methods";
exports.NOTIFICATION_START_TIME = "Notification Start Time";
exports.NOTIFICATION_END_TIME = "Notification End Time";
exports.NOTIFICATION = "Notification";
exports.PREFIX = "Prefix";
// Notification Methods
exports.PUSH_NOTIFICATION = "Push Notification";
exports.CALL_AND_TEXT = "Call & Text";
