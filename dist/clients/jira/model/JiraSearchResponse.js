"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JiraSearchResponse = void 0;
class JiraSearchResponse {
    constructor(apiResponse, constructor) {
        this.startAt = apiResponse.startAt;
        this.maxResults = apiResponse.maxResults;
        this.total = apiResponse.total;
        this.issues = apiResponse.issues;
        this.names = apiResponse.names;
        if (!Array.isArray(apiResponse.issues)) {
            throw new Error();
        }
        this.issues = [];
        for (let i = 0; i < apiResponse.issues.length; i++) {
            let habitEpic = new constructor(apiResponse.issues[i], this.names);
            this.issues.push(habitEpic);
        }
    }
}
exports.JiraSearchResponse = JiraSearchResponse;
