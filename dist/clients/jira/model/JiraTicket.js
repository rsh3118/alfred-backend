"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JiraTicket = void 0;
const Logger_1 = require("../../../util/Logger");
class JiraTicket {
    constructor(apiResponse, keyObject) {
        let logger = new Logger_1.Logger("JiraTicket: constructor", false);
        this.id = apiResponse.id;
        this.key = apiResponse.key;
        this.summary = apiResponse.fields.summary;
        this.status = apiResponse.fields.status.name;
        this.fieldsMap = new Map();
        let sampleMap = new Map();
        logger.info("fields", apiResponse.fields);
        logger.info("keyObject", keyObject);
        let fieldIds = Object.getOwnPropertyNames(apiResponse.fields);
        for (let i = 0; i < fieldIds.length; i++) {
            let fieldId = fieldIds[i];
            logger.info(fieldId);
            let fieldName = keyObject[fieldId];
            logger.info(fieldName);
            logger.info(apiResponse.fields[fieldId]);
            if (fieldName) {
                this.fieldsMap.set(fieldName, apiResponse.fields[fieldId]);
                sampleMap.set("5", "5");
                logger.info("fielderino", this.fieldsMap.get(fieldName));
                logger.info("sampleMap", sampleMap);
            }
        }
        //logger.info("fieldMap", this.fieldsMap);
    }
}
exports.JiraTicket = JiraTicket;
