"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlertConfigurationTicket = void 0;
const JiraTicket_1 = require("./JiraTicket");
const TimeOfDay_1 = require("../../../model/TimeOfDay");
const dayjs_1 = __importDefault(require("dayjs"));
const Logger_1 = require("../../../util/Logger");
const JiraConstants_1 = require("./JiraConstants");
const DateUtil_1 = require("../../../util/DateUtil");
class AlertConfigurationTicket extends JiraTicket_1.JiraTicket {
    constructor(apiResponseIssue, keyObject) {
        super(apiResponseIssue, keyObject);
        this.alarmStartTimeOfDay = new TimeOfDay_1.TimeOfDay("00:00");
        this.alarmEndTimeofDay = new TimeOfDay_1.TimeOfDay("23:59");
        this.shouldCallAndText = false;
        this.shouldNotify = false;
        this.prefix = 'Default';
        this.snoozeUntil = null;
        let logger = new Logger_1.Logger("AlertConfigurationTicket:constructor", false);
        let debugLogger = new Logger_1.Logger("(debug) AlertConfigurationTicket:constructor", false);
        //logger.info("fields", this.fieldsMap)
        debugLogger.info(JiraConstants_1.NOTIFICATION_START_TIME, this.fieldsMap.get(JiraConstants_1.NOTIFICATION_START_TIME));
        this.alarmStartTimeOfDay = new TimeOfDay_1.TimeOfDay(this.fieldsMap.get(JiraConstants_1.NOTIFICATION_START_TIME));
        debugLogger.info(JiraConstants_1.NOTIFICATION_END_TIME, this.fieldsMap.get(JiraConstants_1.NOTIFICATION_END_TIME));
        this.alarmEndTimeofDay = new TimeOfDay_1.TimeOfDay(this.fieldsMap.get(JiraConstants_1.NOTIFICATION_END_TIME));
        debugLogger.info(JiraConstants_1.SNOOZE_UNTIL, this.fieldsMap.get(JiraConstants_1.SNOOZE_UNTIL));
        let snoozeUntilString = this.fieldsMap.get(JiraConstants_1.SNOOZE_UNTIL);
        snoozeUntilString = snoozeUntilString.substring(0, snoozeUntilString.length - 5);
        debugLogger.info("snooze until string", snoozeUntilString);
        this.snoozeUntil = (0, DateUtil_1.convertToDate)(snoozeUntilString, 'YYYY-MM-DDTYY:HH.mm');
        debugLogger.info("snooze until", this.snoozeUntil);
        debugLogger.info("current time", (0, dayjs_1.default)());
        debugLogger.info(JiraConstants_1.ALERTING_METHODS, this.fieldsMap.get(JiraConstants_1.ALERTING_METHODS));
        debugLogger.info(JiraConstants_1.NOTIFICATION, this.fieldsMap.get(JiraConstants_1.NOTIFICATION).map((obj) => obj.value));
        let notificationMethods = this.fieldsMap.get(JiraConstants_1.NOTIFICATION).map((obj) => obj.value);
        this.shouldCallAndText = notificationMethods.includes(JiraConstants_1.CALL_AND_TEXT);
        this.shouldNotify = notificationMethods.includes(JiraConstants_1.PUSH_NOTIFICATION);
        this.prefix = this.fieldsMap.get(JiraConstants_1.PREFIX);
        logger.info("alertConfig", {
            "alarmStartTimeOfDay": this.alarmStartTimeOfDay,
            "alarmEndTimeofDay": this.alarmEndTimeofDay,
            "shouldCallAndText": this.shouldCallAndText,
            "shouldNotify": this.shouldNotify,
            "prefix": this.prefix,
            "snoozeUntil": this.snoozeUntil
        });
    }
}
exports.AlertConfigurationTicket = AlertConfigurationTicket;
