"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HabitEpic = void 0;
const Cadence_1 = require("./Cadence");
const JiraTicket_1 = require("./JiraTicket");
class HabitEpic extends JiraTicket_1.JiraTicket {
    constructor(apiResponseIssue, keyObject) {
        super(apiResponseIssue, keyObject);
        this.alarmStartTimeInMinutes = this.fieldsMap.get("Daily Start Time");
        this.alarmEndTimeInMinutes = this.fieldsMap.get("Daily End Time");
        this.cadence = (0, Cadence_1.createCadence)(this.fieldsMap.get("Cadence").value);
        this.priority = this.fieldsMap.get("Priority").name;
    }
}
exports.HabitEpic = HabitEpic;
