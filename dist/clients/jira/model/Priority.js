"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Priority = void 0;
var Priority;
(function (Priority) {
    Priority[Priority["HIGHEST"] = 0] = "HIGHEST";
    Priority[Priority["HIGH"] = 1] = "HIGH";
    Priority[Priority["MEDIUM"] = 2] = "MEDIUM";
    Priority[Priority["LOW"] = 3] = "LOW";
    Priority[Priority["LOWEST"] = 4] = "LOWEST";
})(Priority = exports.Priority || (exports.Priority = {}));
