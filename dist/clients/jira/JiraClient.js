"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JiraClient = void 0;
const Logger_1 = require("../../util/Logger");
const RestAPIConstants_1 = require("../RestAPIConstants");
const JiraSearchResponse_1 = require("./model/JiraSearchResponse");
const Task_1 = require("./model/Task");
const fetch = require('node-fetch');
class JiraClient {
    constructor() {
        this.baseUrl = `https://ritwik.atlassian.net`;
        this.searchEndpoint = `/rest/api/3/search`;
        // async sample(filter: string){
        //   var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
        //   var params = {
        //     jql: `(Sprint is empty) AND issuetype = TASK AND project = Tasks AND (due >= ${startDate.format('YYYY-MM-DD')} AND due <= ${endDate.format('YYYY-MM-DD')}) AND status != Done AND status != Failed`, 
        //     expand: 'names,renderedFields'
        //   }
        //   url.search = new URLSearchParams(params).toString();
        //   var response: Response = await fetch(url, {
        //   method: 'GET',
        //   headers: {
        //       'Authorization': `Basic ${Buffer.from(
        //       'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
        //       ).toString('base64')}`,
        //       'Accept': 'application/json',
        //       'Content-Type': 'application/json'
        //   },
        //   })
        //   try {
        //       let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
        //       return searchResponse.issues
        //   } catch (err) {
        //       console.log(err)
        //       throw err
        //   }
        // }
    }
    callJiraApi(method, endpoint, params, body) {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL(`${this.baseUrl}${endpoint}`);
            if (params) {
                url.search = new URLSearchParams(params).toString();
            }
            let requestDetails = {
                method: method,
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            };
            if (body) {
                requestDetails = Object.assign(Object.assign({}, requestDetails), { body });
            }
            var response = yield fetch(url, requestDetails);
            return response;
        });
    }
    searchIssuesWithoutPagination(filter, startIndex) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("JiraClient:searchIssuesWithoutPagination", false);
            var params = {
                jql: filter,
                expand: 'names,renderedFields',
                startAt: startIndex
            };
            let response = yield this.callJiraApi(RestAPIConstants_1.HttpMethod.GET, this.searchEndpoint, params, null);
            try {
                let searchResponse = new JiraSearchResponse_1.JiraSearchResponse(yield response.json(), Task_1.Task);
                return searchResponse;
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    searchIssues(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("JiraClient:searchIssues", false);
            let tasks = [];
            let currentIndex = 0;
            let totalResults = 0;
            let maxResults = 0;
            do {
                let response = yield this.searchIssuesWithoutPagination(filter, currentIndex);
                logger.info("issues", response.issues.map(issue => issue.key));
                currentIndex = response.startAt;
                totalResults = response.total;
                maxResults = response.maxResults;
                tasks.push(...response.issues);
                logger.info("pagination status", {
                    filter,
                    currentIndex,
                    totalResults,
                    maxResults
                });
                currentIndex = currentIndex + response.issues.length;
            } while (currentIndex < totalResults - 1);
            return tasks;
        });
    }
    addIssuesToSprint(backlogTasks, sprintId) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("JiraClient:addIssuesToSprint", false);
            logger.info("sprintId", sprintId);
            const body = `{
      "issues": [${backlogTasks.map(task => `"${task.key}"`).join(",")}]
    }`;
            yield this.callJiraApi(RestAPIConstants_1.HttpMethod.POST, `/rest/agile/1.0/sprint/${sprintId}/issue`, {}, body);
        });
    }
}
exports.JiraClient = JiraClient;
