"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JiraIssuesMacro = void 0;
const XMLUtil_1 = require("../../../util/XMLUtil");
class JiraIssuesMacro {
    constructor(title, query, columns, limit) {
        this.title = title;
        this.query = query;
        this.columns = columns;
        this.limit = limit;
    }
    renderXML() {
        /*
        <ac:structured-macro ac:name="jira" ac:schema-version="1" data-layout="default" ac:macro-id="48a4d1b0-02ec-4231-82ff-83ac7c7309a6">
                <ac:parameter ac:name="server">System JIRA</ac:parameter>
                <ac:parameter ac:name="columns">key,summary,status</ac:parameter>
                <ac:parameter ac:name="maximumIssues">20</ac:parameter>
                <ac:parameter ac:name="jqlQuery">project =REFLECTION AND created &gt;= &quot;2021/02/08 00:00&quot; AND created &lt;= &quot;2021/02/08 23:59&quot; AND status = &quot;SUCCESS&quot;</ac:parameter>
                <ac:parameter ac:name="serverId">dfdaf529-3f84-3696-83b8-07f5d2c276bd</ac:parameter>
            </ac:structured-macro>
        */
        let lines = [
            `<h2>${(0, XMLUtil_1.replaceEscapeCharacters)(this.title)}</h2>`,
            `<p/>`,
            `<ac:structured-macro ac:name="jira" ac:schema-version="1" data-layout="default" ac:macro-id="48a4d1b0-02ec-4231-82ff-83ac7c7309a6">`,
            `<ac:parameter ac:name="server">System JIRA</ac:parameter>`,
            `<ac:parameter ac:name="columns">${this.columns.join(",")}</ac:parameter>`,
            `<ac:parameter ac:name="maximumIssues">${this.limit}</ac:parameter>`,
            `<ac:parameter ac:name="jqlQuery">${(0, XMLUtil_1.replaceEscapeCharacters)(this.query)}</ac:parameter>`,
            `<ac:parameter ac:name="serverId">dfdaf529-3f84-3696-83b8-07f5d2c276bd</ac:parameter>`,
            `</ac:structured-macro>`,
            `<p/>`,
        ];
        let xml = lines.join(`\n`);
        return xml;
    }
}
exports.JiraIssuesMacro = JiraIssuesMacro;
