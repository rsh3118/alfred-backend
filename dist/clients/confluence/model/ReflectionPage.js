"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DailyReflectionPage = exports.ReflectionPage = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const JiraIssuesMacro_1 = require("./JiraIssuesMacro");
const utc_1 = __importDefault(require("dayjs/plugin/utc"));
const timezone_1 = __importDefault(require("dayjs/plugin/timezone"));
dayjs_1.default.extend(utc_1.default);
dayjs_1.default.extend(timezone_1.default);
dayjs_1.default.tz.setDefault("America/Los_Angeles");
class ReflectionPage {
    constructor() {
        this.macrosList = [];
    }
    renderXML() {
        let xml = this.macrosList.map(macro => macro.renderXML()).join("\n");
        return xml;
    }
}
exports.ReflectionPage = ReflectionPage;
class DailyReflectionPage extends ReflectionPage {
    constructor(date) {
        super();
        this.date = date;
        let successes = new JiraIssuesMacro_1.JiraIssuesMacro(`Successes`, `project = REFLECTION AND created >= "${date.format("YYYY/MM/DD")} 00:00" AND created <= "${date.format("YYYY/MM/DD")}  23:59" AND status = "SUCCESS"`, [`key`, `summary`, `status`], 50);
        let failures = new JiraIssuesMacro_1.JiraIssuesMacro(`Failures`, `project = REFLECTION AND created >= "${date.format("YYYY/MM/DD")} 00:00" AND created <= "${date.format("YYYY/MM/DD")} 23:59" AND status = "FAILURE"`, [`key`, `summary`, `status`], 50);
        let observations = new JiraIssuesMacro_1.JiraIssuesMacro(`Observations`, `project =REFLECTION AND created >= "${date.format("YYYY/MM/DD")} 00:00" AND created <= "${date.format("YYYY/MM/DD")} 23:59" AND status = "OBSERVATION"`, [`key`, `summary`, `status`], 50);
        let tasksCompleted = new JiraIssuesMacro_1.JiraIssuesMacro(`Tasks Completed`, `project != Acts AND resolved >= "${date.format("YYYY/MM/DD")} 00:00" AND resolved <= "${date.format("YYYY/MM/DD")} 23:59"`, [`key`, `summary`, `status`], 50);
        let habits = new JiraIssuesMacro_1.JiraIssuesMacro(`Habits`, `project = HABIT AND text ~ "${date.format("MM/DD/YY")}"`, [`key`, `summary`, `status`], 50);
        let activities = new JiraIssuesMacro_1.JiraIssuesMacro(`Activities`, `project = ACT AND issuetype = Activity AND summary ~ "${date.format("MM/DD/YY")}"`, [`key`, `summary`, `Efficiency`], 50);
        this.macrosList = [successes, failures, observations, tasksCompleted, habits, activities];
    }
}
exports.DailyReflectionPage = DailyReflectionPage;
