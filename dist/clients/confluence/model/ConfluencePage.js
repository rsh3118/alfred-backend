"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfluencePage = void 0;
const node_html_parser_1 = require("node-html-parser");
class ConfluencePage {
    constructor(apiResponse) {
        this.id = apiResponse.id;
        this.type = apiResponse.type;
        this.status = apiResponse.status;
        this.title = apiResponse.title;
        var doc = (0, node_html_parser_1.parse)('<p>Woke up to hear that an Atlassian wide change on the classification of the data classification of AAID from restricted identifier &rarr; pseudonymous identifier was instigated by me pushing the privacy and security for a more wholistic approach on this (mostly cause they said) we couldn&rsquo;t log it<br /><p/><br />Ganondorf was happy with the change and all the members of the team gave me kudos which was nice.<br /><br />I&rsquo;m also happy because Ganondorf found out about the change from his colleagues and said I was living the Atlassian value of &ldquo;Be the change you seek&rdquo;</p>');
        var doc2 = (0, node_html_parser_1.parse)(apiResponse.body.storage.value);
        this.content = doc2;
    }
}
exports.ConfluencePage = ConfluencePage;
