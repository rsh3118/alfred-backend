"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SearchResultContent = exports.SearchResult = exports.createSearchResultArray = exports.ConfluenceSearchResponse = void 0;
class ConfluenceSearchResponse {
    constructor(apiResponse) {
        this.results = createSearchResultArray(apiResponse.results);
        this.start = apiResponse.start;
        this.limit = apiResponse.limit;
        this.size = apiResponse.size;
        this.cqlQuery = apiResponse.cqlQuery;
    }
}
exports.ConfluenceSearchResponse = ConfluenceSearchResponse;
function createSearchResultArray(apiResponse) {
    if (!Array.isArray(apiResponse)) {
        throw new Error("Search Response results are not an array");
    }
    var resultArray = [];
    apiResponse.forEach((value, index, array) => {
        try {
            resultArray.push(new SearchResult(value));
        }
        catch (err) {
        }
    });
    return resultArray;
}
exports.createSearchResultArray = createSearchResultArray;
class SearchResult {
    constructor(apiResponse) {
        this.content = new SearchResultContent(apiResponse.content);
        this.entityType = apiResponse.entityType;
        this.title = apiResponse.title;
        this.url = apiResponse.url;
        this.lastModified = new Date(apiResponse.lastModified);
    }
}
exports.SearchResult = SearchResult;
class SearchResultContent {
    constructor(apiResponse) {
        this.id = apiResponse.id;
        this.type = apiResponse.type;
        this.status = apiResponse.status;
        this.title = apiResponse.title;
    }
}
exports.SearchResultContent = SearchResultContent;
