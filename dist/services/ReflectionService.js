"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReflectionService = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const ConfluenceClient_1 = require("../clients/confluence/ConfluenceClient");
const ReflectionPage_1 = require("../clients/confluence/model/ReflectionPage");
const JiraService_1 = require("./JiraService");
const OpsgenieClient_1 = require("../clients/OpsgenieClient");
const utc_1 = __importDefault(require("dayjs/plugin/utc"));
const timezone_1 = __importDefault(require("dayjs/plugin/timezone"));
dayjs_1.default.extend(utc_1.default);
dayjs_1.default.extend(timezone_1.default);
dayjs_1.default.tz.setDefault("America/Los_Angeles");
class ReflectionService {
    constructor() {
        this.confluenceClient = new ConfluenceClient_1.ConfluenceClient();
        this.jiraClient = new JiraService_1.JiraService();
        this.opsgenieClient = new OpsgenieClient_1.OpsgenieClient();
    }
    createDailyReflectionPage() {
        return __awaiter(this, void 0, void 0, function* () {
            let currentDate = (0, dayjs_1.default)().tz();
            //currentDate = convertToCST(currentDate)
            let shouldCreateTodaysReflectionPage = !(yield this.hasReflectionPageAlreadyBeenPublished(currentDate));
            if (shouldCreateTodaysReflectionPage) {
                let todaysReflectionPage = new ReflectionPage_1.DailyReflectionPage(currentDate);
                yield this.confluenceClient.createReflectionPage(`${currentDate.format("MM/DD/YY")} Reflection`, todaysReflectionPage.renderXML());
            }
            else {
            }
        });
    }
    hasReflectionPageAlreadyBeenPublished(date) {
        return __awaiter(this, void 0, void 0, function* () {
            let existingPageIds = yield this.confluenceClient.getDailyReflectionPageByDate(date);
            return existingPageIds.length > 0;
        });
    }
}
exports.ReflectionService = ReflectionService;
