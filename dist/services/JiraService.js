"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JiraService = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const HabitEpic_1 = require("../clients/jira/model/HabitEpic");
const JiraSearchResponse_1 = require("../clients/jira/model/JiraSearchResponse");
const Task_1 = require("../clients/jira/model/Task");
var JiraApi = require('jira-client');
// This code sample uses the 'node-fetch' library:
// https://www.npmjs.com/package/node-fetch
const fetch = require('node-fetch');
const utc_1 = __importDefault(require("dayjs/plugin/utc"));
const timezone_1 = __importDefault(require("dayjs/plugin/timezone"));
const AlertConfigurationTicket_1 = require("../clients/jira/model/AlertConfigurationTicket");
const Logger_1 = require("../util/Logger");
const JiraClient_1 = require("../clients/jira/JiraClient");
dayjs_1.default.extend(utc_1.default);
dayjs_1.default.extend(timezone_1.default);
dayjs_1.default.tz.setDefault("America/Los_Angeles");
class JiraService {
    constructor() {
        this.jiraClient = new JiraClient_1.JiraClient();
        // Initialize
        var jira = new JiraApi({
            protocol: 'https',
            host: 'ritwik.atlassian.net',
            username: 'rsh3118@gmail.com',
            password: 'l4IcH8348aOwLNg05CKi0F5D',
            apiVersion: '2',
            strictSSL: true
        });
    }
    /**
     * Returns the meta data for creating issues. This includes the available projects, issue types and fields, including field types and whether or not those fields are required.
     * Projects will not be returned if the user does not have permission to create issues in that project.
     * The fields in the createmeta correspond to the fields in the create screen for the project/issuetype. Fields not in the screen will not be in the createmeta.
     * Fields will only be returned if expand=projects.issuetypes.fields.
     * The results can be filtered by project and/or issue type, given by the query params.
     */
    getIssueCreateMeta() {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/api/3/issue/createmeta');
            var params = { expand: 'projects.issuetypes.fields' };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            try {
                let text = yield response.text();
            }
            catch (err) {
                console.log(err);
            }
        });
    }
    setStoryPointsForTicket(ticket, storyPoints) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("JiraClient:setStoryPointsForTicket", true);
            logger.info("ticket key", ticket.key);
            logger.info("story points", storyPoints);
            const bodyData = `{
      "fields": { 
          "Story Points" : ${storyPoints}
      }
    }`;
            var url = new URL(`https://ritwik.atlassian.net/rest/api/3/issue/${ticket.key}`);
            var response = yield fetch(url, {
                method: 'PUT',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: bodyData
            });
            try {
                //logger.info(`Response: ${response.status} ${response.statusText}`);
                let res = yield response.json();
                console.log(res);
            }
            catch (err) {
                //logger.error("error", err)
                throw err;
            }
        });
    }
    setDueDateForTicket(ticket, dueDate) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("JiraClient:setStoryPointsForTicket", true);
            logger.info("ticket key", ticket.key);
            let todaysDate = `${dueDate.format('YYYY-MM-DD')}`;
            logger.info("todays date", todaysDate);
            const bodyData = `{
      "fields":{
        "duedate" : "${todaysDate}"
      }
    }`;
            var url = new URL(`https://ritwik.atlassian.net/rest/api/3/issue/${ticket.key}`);
            var response = yield fetch(url, {
                method: 'PUT',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: bodyData
            });
            try {
                //logger.info(`Response: ${response.status} ${response.statusText}`);
                //let res = await response.json()
                //console.log(res)
            }
            catch (err) {
                //logger.error("error", err)
                throw err;
            }
        });
    }
    getCurrentSprintTasks() {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/agile/1.0/sprint/25/issue');
            var params = { expand: 'names' };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            try {
                let searchResponse = new JiraSearchResponse_1.JiraSearchResponse(yield response.json(), Task_1.Task);
                return searchResponse.issues;
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    getSprintTasks(sprintId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.jiraClient.searchIssues(`Sprint = ${sprintId}`);
        });
    }
    getSprintTasksWithDateFilter(sprintId, startDate, endDate) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.jiraClient.searchIssues(`Sprint = ${sprintId} AND (due >= ${startDate.format('YYYY-MM-DD')} AND due <= ${endDate.format('YYYY-MM-DD')})`);
        });
    }
    getAlertConfiguration(key) {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/api/3/search');
            var params = {
                jql: `project = CONFIG AND key="${key}"`,
                expand: 'names,renderedFields'
            };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            let responseBody = yield response.json();
            try {
                let searchResponse = new JiraSearchResponse_1.JiraSearchResponse(responseBody, AlertConfigurationTicket_1.AlertConfigurationTicket);
                return searchResponse.issues[0];
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    getSprints() {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/agile/1.0/board/1/sprint');
            var params = { expand: 'names' };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
        });
    }
    getHabitsProjectIssues() {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/api/3/search');
            var params = { jql: 'project=Habits' };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            try {
                let text = yield response.text();
            }
            catch (err) {
                console.log(err);
            }
        });
    }
    getHabitsProjectsEpics() {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/api/3/search');
            var params = {
                jql: 'project=Habits and type = Epic and status = "To do"',
                expand: 'names,renderedFields'
            };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            try {
                let searchResponse = new JiraSearchResponse_1.JiraSearchResponse(yield response.json(), HabitEpic_1.HabitEpic);
                return searchResponse.issues;
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    getTasksForHabitEpic(habitEpic, date) {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/api/3/search');
            var params = {
                jql: `project=Habits and parent in ("${habitEpic.key}")`,
                expand: 'names,renderedFields'
            };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            try {
                let searchResponse = new JiraSearchResponse_1.JiraSearchResponse(yield response.json(), Task_1.Task);
                return searchResponse.issues;
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    getTasksWithoutStoryPoints() {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/api/3/search');
            var params = {
                jql: `project = Tasks AND "Story Points" is empty`,
                expand: 'names,renderedFields'
            };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            try {
                let searchResponse = new JiraSearchResponse_1.JiraSearchResponse(yield response.json(), Task_1.Task);
                return searchResponse.issues;
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    getTasksWithoutDueDate() {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL('https://ritwik.atlassian.net/rest/api/3/search');
            var params = {
                jql: `project = Tasks AND duedate is empty AND issuetype = TASK`,
                expand: 'names,renderedFields'
            };
            url.search = new URLSearchParams(params).toString();
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            try {
                let searchResponse = new JiraSearchResponse_1.JiraSearchResponse(yield response.json(), Task_1.Task);
                return searchResponse.issues;
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    getTasksInBacklog(startDate, endDate) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.jiraClient.searchIssues(`(Sprint is empty) AND issuetype = TASK AND project = Tasks AND (due >= ${startDate.format('YYYY-MM-DD')} AND due <= ${endDate.format('YYYY-MM-DD')}) AND status != Done AND status != Failed`);
            // var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
            //   var params = {
            //     jql: `(Sprint is empty) AND issuetype = TASK AND project = Tasks AND (due >= ${startDate.format('YYYY-MM-DD')} AND due <= ${endDate.format('YYYY-MM-DD')}) AND status != Done AND status != Failed`, 
            //     expand: 'names,renderedFields'
            //   }
            //   url.search = new URLSearchParams(params).toString();
            //   var response: Response = await fetch(url, {
            //   method: 'GET',
            //   headers: {
            //       'Authorization': `Basic ${Buffer.from(
            //       'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
            //       ).toString('base64')}`,
            //       'Accept': 'application/json',
            //       'Content-Type': 'application/json'
            //   },
            //   })
            //   try {
            //       let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
            //       return searchResponse.issues
            //   } catch (err) {
            //       console.log(err)
            //       throw err
            //   }
        });
    }
    createHabitTask(parentKey, summary, dueDate) {
        return __awaiter(this, void 0, void 0, function* () {
            let bodyData = `{
      "update": {},
      "fields": {
        "summary": "${summary}",
        "parent": {
          "key": "${parentKey}"
        },
        "issuetype": {
          "id": "10016"
        },
        "project": {
          "id": "10005"
        },
        "reporter": {
          "id": "557058:2f12732f-687c-4396-9b05-c1fe99179612"
        },
        "duedate": "${dueDate}",
        "assignee": {
          "id": "557058:2f12732f-687c-4396-9b05-c1fe99179612"
        }
      }
    }`;
            var url = new URL('https://ritwik.atlassian.net/rest/api/3/issue');
            var response = yield fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: bodyData
            });
            try {
                let res = yield response.json();
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    getTransitions(taskKey) {
        return __awaiter(this, void 0, void 0, function* () {
            var url = new URL(`https://ritwik.atlassian.net/rest/api/3/issue/${taskKey}/transitions`);
            var response = yield fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                },
            });
            try {
                let res = yield response.json();
                console.log(res);
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    setHabitTaskStatusToFailed(taskKey) {
        return __awaiter(this, void 0, void 0, function* () {
            const bodyData = `{
      "update": {
        "comment": [
          {
            "add": {
              "body": {
                "type": "doc",
                "version": 1,
                "content": [
                  {
                    "type": "paragraph",
                    "content": [
                      {
                        "text": "Issue has expired and has not been completed marking it as failed",
                        "type": "text"
                      }
                    ]
                  }
                ]
              }
            }
          }
        ]
      },
      "transition": {
        "id": "21"
      }
    }`;
            var url = new URL(`https://ritwik.atlassian.net/rest/api/3/issue/${taskKey}/transitions`);
            var response = yield fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: bodyData
            });
            try {
                let res = yield response.json();
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    moveIssuesToBacklogNonTransactional(backlogTasks) {
        return __awaiter(this, void 0, void 0, function* () {
            const bodyData = `{
      "issues": [${backlogTasks.map(task => `"${task.key}"`).join(",")}]
    }`;
            var url = new URL(`https://ritwik.atlassian.net/rest/agile/1.0/backlog/issue`);
            try {
                let requestDetails = {
                    method: 'POST',
                    headers: {
                        'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: bodyData
                };
                var response = yield fetch(`https://ritwik.atlassian.net/rest/agile/1.0/backlog/issue`, Object.assign({}, requestDetails));
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    moveIssuesToSprintNonTransactional(backlogTasks, sprintId) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("JiraClient:moveIssuesToSprint", true);
            logger.info("sprintId", sprintId);
            const bodyData = `{
      "issues": [${backlogTasks.map(task => `"${task.key}"`).join(",")}]
    }`;
            var url = new URL(`https://ritwik.atlassian.net/rest/agile/1.0/sprint/${sprintId}/issue`);
            logger.info("url", url);
            try {
                let requestDetails = {
                    method: 'POST',
                    headers: {
                        'Authorization': `Basic ${Buffer.from('rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D').toString('base64')}`,
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: bodyData
                };
                var response = yield fetch(url, Object.assign({}, requestDetails));
                logger.info("response", `Response: ${response.status} ${response.statusText}`);
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
    // async moveIssuesToSprintNonTransactional(backlogTasks: Task[], sprintId: string) {
    //   let logger = new Logger("JiraClient:moveIssuesToSprint", false)
    //   logger.info("sprintId", sprintId)
    //   const bodyData = `{
    //     "issues": [${backlogTasks.map(task => `"${task.key}"`).join(",")}]
    //   }`;
    //   var url = new URL(`https://ritwik.atlassian.net/rest/agile/1.0/sprint/${sprintId}/issue`)
    //   logger.info("url", url)
    //   try {
    //       let requestDetails = {
    //         method: 'POST',
    //         headers: {
    //             'Authorization': `Basic ${Buffer.from(
    //             'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
    //             ).toString('base64')}`,
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json'
    //         },
    //         body: bodyData
    //         }
    //       var response: Response = await fetch(url, {...requestDetails})
    //       logger.info("response", `Response: ${response.status} ${response.statusText}`)
    //   } catch (err) {
    //       console.log(err)
    //       throw err
    //   }
    // }
    moveIssuesToSprint(tasksToRemove, sprintId) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("JiraService:moveIssuesToSprint", true);
            let currentIndex = 0;
            let size = 25;
            let total = tasksToRemove.length;
            let tasks = tasksToRemove.slice(currentIndex, currentIndex + size);
            do {
                let tasks = tasksToRemove.slice(currentIndex, currentIndex + size);
                logger.info("pagination status", {
                    currentIndex,
                    size,
                    total
                });
                yield this.jiraClient.addIssuesToSprint(tasks, sprintId);
                var setOfTaskKeysMoved = new Set(tasks.map(task => task.key));
                let setOfTaskKeysInSprint = new Set((yield this.getSprintTasks(sprintId)).map(task => task.key));
                let allTasksAreInSprint = true;
                do {
                    yield Promise.all([...setOfTaskKeysMoved].map((taskKey) => __awaiter(this, void 0, void 0, function* () {
                        allTasksAreInSprint = allTasksAreInSprint && setOfTaskKeysInSprint.has(taskKey);
                        setOfTaskKeysInSprint = new Set((yield this.getSprintTasks(sprintId)).map(task => task.key));
                    })));
                } while (!allTasksAreInSprint);
                currentIndex = currentIndex + size;
            } while (currentIndex < total);
        });
    }
    moveIssuesToBacklog(tasks) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.moveIssuesToBacklogNonTransactional(tasks);
            var setOfTaskKeysMoved = new Set(tasks.map(task => task.key));
            let farInThePastDay = (0, dayjs_1.default)("12-25-1995", "MM-DD-YYYY");
            let farInTheFutureDay = (0, dayjs_1.default)("12-25-2055", "MM-DD-YYYY");
            let setOfTaskKeysInSprint = new Set((yield this.getTasksInBacklog(farInThePastDay, farInTheFutureDay)).map(task => task.key));
            let allTasksAreInSprint = true;
            do {
                yield Promise.all([...setOfTaskKeysMoved].map((taskKey) => __awaiter(this, void 0, void 0, function* () {
                    allTasksAreInSprint = allTasksAreInSprint && setOfTaskKeysInSprint.has(taskKey);
                    setOfTaskKeysInSprint = new Set((yield this.getTasksInBacklog(farInThePastDay, farInTheFutureDay)).map(task => task.key));
                })));
            } while (!allTasksAreInSprint);
        });
    }
}
exports.JiraService = JiraService;
