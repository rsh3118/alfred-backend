"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlertingService = exports.AlertPriority = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const JiraService_1 = require("./JiraService");
const OpsgenieClient_1 = require("../clients/OpsgenieClient");
const utc_1 = __importDefault(require("dayjs/plugin/utc"));
const timezone_1 = __importDefault(require("dayjs/plugin/timezone"));
const JiraConstants_1 = require("../clients/jira/model/JiraConstants");
const TimeOfDay_1 = require("../model/TimeOfDay");
const Logger_1 = require("../util/Logger");
dayjs_1.default.extend(utc_1.default);
dayjs_1.default.extend(timezone_1.default);
dayjs_1.default.tz.setDefault("America/Los_Angeles");
var AlertPriority;
(function (AlertPriority) {
    AlertPriority["HIGH"] = "HIGH";
    AlertPriority["MEDIUM"] = "MEDIUM";
    AlertPriority["LOW"] = "PRIORITY";
})(AlertPriority = exports.AlertPriority || (exports.AlertPriority = {}));
class DefaultAlertConfiguration {
    constructor() {
        this.alarmStartTimeOfDay = new TimeOfDay_1.TimeOfDay("00:00");
        this.alarmEndTimeofDay = new TimeOfDay_1.TimeOfDay("23:59");
        this.shouldCallAndText = true;
        this.shouldNotify = true;
        this.prefix = 'Default';
        this.snoozeUntil = null;
    }
}
class AlertingService {
    constructor() {
        this.jiraClient = new JiraService_1.JiraService();
        this.opsgenieClient = new OpsgenieClient_1.OpsgenieClient();
        this.lowPriorityAlertConfiguration = new DefaultAlertConfiguration();
        this.mediumPriorityAlertConfiguration = new DefaultAlertConfiguration();
        this.highPriorityAlertConfiguration = new DefaultAlertConfiguration();
        this.setAlarmConfigurations();
    }
    setAlarmConfigurations() {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("AlertingService:setAlarmConfigurations", false);
            this.lowPriorityAlertConfiguration = yield this.jiraClient.getAlertConfiguration(JiraConstants_1.LOW_PRIORITY_ALERT_CONFIG_KEY);
            this.mediumPriorityAlertConfiguration = yield this.jiraClient.getAlertConfiguration(JiraConstants_1.MEDIUM_PRIORITY_ALERT_CONFIG_KEY);
            this.highPriorityAlertConfiguration = yield this.jiraClient.getAlertConfiguration(JiraConstants_1.HIGH_PRIORITY_ALERT_CONFIG_KEY);
            logger.info("configurations loaded from jira", {
                "lowPriority": this.lowPriorityAlertConfiguration,
                "mediumPriority": this.mediumPriorityAlertConfiguration,
                "highPriority": this.highPriorityAlertConfiguration,
            });
        });
    }
    sendAlert(priority, message) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("AlertingService:sendAlert", false);
            let config;
            switch (priority) {
                case AlertPriority.HIGH:
                    config = this.highPriorityAlertConfiguration;
                    break;
                case AlertPriority.MEDIUM:
                    config = this.mediumPriorityAlertConfiguration;
                    break;
                case AlertPriority.LOW:
                    config = this.lowPriorityAlertConfiguration;
                    break;
                default:
                    throw new Error("must specify a priority");
            }
            logger.info("config", config);
            if (!this.canAlertAtCurrentTime(config)) {
                return;
            }
            if (config.shouldCallAndText) {
                this.opsgenieClient.createAlert(OpsgenieClient_1.OpsgenieAction.CALL_AND_TEXT, `[${config.prefix}] ${message}`);
            }
            if (config.shouldNotify) {
                this.opsgenieClient.createAlert(OpsgenieClient_1.OpsgenieAction.PUSH_NOTIFICATION, `[${config.prefix}] ${message}`);
            }
        });
    }
    canAlertAtCurrentTime(config) {
        let logger = new Logger_1.Logger("AlertingService:canAlertAtCurrentTime", false);
        logger.info("config", config);
        let currentTime = (0, dayjs_1.default)();
        logger.info("current time", currentTime);
        logger.info("current hour", currentTime.hour());
        logger.info("current minute", currentTime.minute());
        logger.info("snooze until time", config.snoozeUntil);
        if (config.snoozeUntil && config.snoozeUntil > currentTime) {
            logger.info("config.snoozeUntil > currentTime", config.snoozeUntil > currentTime);
            return false;
        }
        logger.info("config alarm start time of day hour", config.alarmStartTimeOfDay.hour);
        logger.info("config alarm start time of day minute", config.alarmStartTimeOfDay.minute);
        logger.info("config alarm start time of day minute", ` ${config.alarmStartTimeOfDay.minute} `);
        logger.info("config alarm end time of day hour", config.alarmEndTimeofDay.hour);
        logger.info("config alarm end time of day minute", config.alarmEndTimeofDay.minute);
        let currentHour = currentTime.hour();
        let currentMinute = currentTime.minute();
        let currentMinuteTime = currentHour * 60 + currentMinute;
        let startMinuteTime = config.alarmStartTimeOfDay.hour * 60 + config.alarmStartTimeOfDay.minute;
        let endMinuteTime = config.alarmEndTimeofDay.hour * 60 + config.alarmEndTimeofDay.minute;
        logger.info("currentMinute", currentMinuteTime);
        logger.info("startMinuteTime", startMinuteTime);
        logger.info("endMinuteTime", endMinuteTime);
        if (startMinuteTime <= currentMinuteTime && currentMinuteTime <= endMinuteTime) {
            logger.info("currentHour < config.alarmStartTimeOfDay.hour", currentHour < config.alarmStartTimeOfDay.hour);
            return true;
        }
        return false;
    }
}
exports.AlertingService = AlertingService;
