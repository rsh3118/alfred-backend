"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HabitTrackingService = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const ConfluenceClient_1 = require("../clients/confluence/ConfluenceClient");
const JiraService_1 = require("./JiraService");
const Cadence_1 = require("../clients/jira/model/Cadence");
const Status_1 = require("../clients/jira/model/Status");
const OpsgenieClient_1 = require("../clients/OpsgenieClient");
const DateUtil_1 = require("../util/DateUtil");
const utc_1 = __importDefault(require("dayjs/plugin/utc"));
const timezone_1 = __importDefault(require("dayjs/plugin/timezone"));
dayjs_1.default.extend(utc_1.default);
dayjs_1.default.extend(timezone_1.default);
dayjs_1.default.tz.setDefault("America/Los_Angeles");
class HabitTrackingService {
    constructor() {
        this.confluenceClient = new ConfluenceClient_1.ConfluenceClient();
        this.jiraClient = new JiraService_1.JiraService();
        this.opsgenieClient = new OpsgenieClient_1.OpsgenieClient();
    }
    createDailyHabits() {
        return __awaiter(this, void 0, void 0, function* () {
            let currentDate = (0, dayjs_1.default)().tz();
            let habitEpics = yield this.jiraClient.getHabitsProjectsEpics();
            habitEpics.forEach((habitEpic) => __awaiter(this, void 0, void 0, function* () {
                let habitTasks = yield (yield this.jiraClient.getTasksForHabitEpic(habitEpic, (0, dayjs_1.default)().tz()));
                if (habitTasks.length > 0) {
                    habitTasks.sort((a, b) => a.dueDate.valueOf() - b.dueDate.valueOf());
                    let mostRecentTask = habitTasks[habitTasks.length - 1];
                    if (this.shouldNewTaskBeCreated(habitEpic.cadence, mostRecentTask.dueDate, currentDate)) {
                        this.jiraClient.createHabitTask(habitEpic.key, `${habitEpic.summary} ${currentDate.format('MM/DD/YY')}`, `${currentDate.format("YYYY-MM-DD")}`);
                    }
                    else {
                    }
                }
                else {
                    this.jiraClient.createHabitTask(habitEpic.key, `${habitEpic.summary} ${currentDate.format('MM/DD/YY')}`, `${currentDate.format("YYYY-MM-DD")}`);
                }
            }));
        });
    }
    expireOldHabitTasks() {
        return __awaiter(this, void 0, void 0, function* () {
            let currentDate = (0, dayjs_1.default)().tz();
            let habitEpics = yield this.jiraClient.getHabitsProjectsEpics();
            habitEpics.forEach((habitEpic) => __awaiter(this, void 0, void 0, function* () {
                let habitTasks = yield (yield this.jiraClient.getTasksForHabitEpic(habitEpic, (0, dayjs_1.default)().tz()));
                habitTasks.forEach(task => {
                    if (task.status == Status_1.Status.TODO && this.isHabitTaskExpired(habitEpic.cadence, task.dueDate, currentDate)) {
                        this.jiraClient.setHabitTaskStatusToFailed(task.key);
                    }
                });
            }));
        });
    }
    isHabitTaskExpired(cadence, lastTaskDueDate, currentDate) {
        if (cadence instanceof Cadence_1.DailyCadence) {
            let cutoff = lastTaskDueDate.add(1, 'day');
            let res = cutoff.valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.WeeklyCadence) {
            let res = lastTaskDueDate.add(3, 'day').add(12, 'hour').valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.MonthlyCadence) {
            let res = lastTaskDueDate.add(15, 'day').valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.EveryXDaysCadence) {
            let res = lastTaskDueDate.add(cadence.x * 0.5, 'day').valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.DayOfWeekCadence) {
            let res = lastTaskDueDate.add(3, 'day').add(12, 'hour').valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.DayOfTheMonthCadence) {
            let res = lastTaskDueDate.add(15, 'day').valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.LastDayOfTheMonthCadence) {
            let res = lastTaskDueDate.add(15, 'day').valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.WeekdayCadence) {
            let res = lastTaskDueDate.add(1, 'day').add(12, 'hour').valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.WeekendCadence) {
            let res = lastTaskDueDate.add(1, 'day').add(12, 'hour').valueOf() < currentDate.valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.EverydayExceptFridayCadence) {
            let res = lastTaskDueDate.add(1, 'day').add(12, 'hour').valueOf() < currentDate.valueOf();
            return res;
        }
        throw new Error("should be impossible to reach");
    }
    shouldNewTaskBeCreated(cadence, lastTaskDueDate, currentDate) {
        if (cadence instanceof Cadence_1.DailyCadence) {
            let res = !(0, DateUtil_1.isSameDay)(lastTaskDueDate, currentDate);
            return res;
        }
        if (cadence instanceof Cadence_1.WeeklyCadence) {
            let res = !(0, DateUtil_1.isSameWeek)(lastTaskDueDate, currentDate);
            return res;
        }
        if (cadence instanceof Cadence_1.MonthlyCadence) {
            let res = !(0, DateUtil_1.isSameMonth)(lastTaskDueDate, currentDate);
            return res;
        }
        if (cadence instanceof Cadence_1.EveryXDaysCadence) {
            let res = lastTaskDueDate.startOf('day').add(cadence.x, 'day').subtract(1, 'second').valueOf() < currentDate.startOf('day').valueOf();
            return res;
        }
        if (cadence instanceof Cadence_1.DayOfWeekCadence) {
            let res = !(0, DateUtil_1.isSameDay)(lastTaskDueDate, currentDate) && currentDate.day() === cadence.getDayOfWeekNumber();
            return res;
        }
        if (cadence instanceof Cadence_1.DayOfTheMonthCadence) {
            let res = !(0, DateUtil_1.isSameDay)(lastTaskDueDate, currentDate) && Number(currentDate.format('DD')) === Number(cadence.dayOfMonth.toString());
            return res;
        }
        if (cadence instanceof Cadence_1.LastDayOfTheMonthCadence) {
            let res = !(0, DateUtil_1.isSameDay)(lastTaskDueDate, currentDate) && (0, DateUtil_1.isEndOfMonth)(currentDate);
            return res;
        }
        if (cadence instanceof Cadence_1.WeekdayCadence) {
            let res = !(0, DateUtil_1.isSameDay)(lastTaskDueDate, currentDate) && (0, DateUtil_1.isWeekday)(currentDate);
            return res;
        }
        if (cadence instanceof Cadence_1.WeekendCadence) {
            let res = !(0, DateUtil_1.isSameDay)(lastTaskDueDate, currentDate) && (0, DateUtil_1.isWeekend)(currentDate);
            return res;
        }
        if (cadence instanceof Cadence_1.EverydayExceptFridayCadence) {
            let res = !(0, DateUtil_1.isSameDay)(lastTaskDueDate, currentDate) && !(0, DateUtil_1.isFriday)(currentDate);
            return res;
        }
        throw new Error("should be impossible to reach");
    }
}
exports.HabitTrackingService = HabitTrackingService;
