"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SprintService = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const ConfluenceClient_1 = require("../clients/confluence/ConfluenceClient");
const JiraService_1 = require("./JiraService");
const utc_1 = __importDefault(require("dayjs/plugin/utc"));
const timezone_1 = __importDefault(require("dayjs/plugin/timezone"));
const Status_1 = require("../clients/jira/model/Status");
const DateUtil_1 = require("../util/DateUtil");
const Logger_1 = require("../util/Logger");
const AlertingService_1 = require("./AlertingService");
const JiraConstants_1 = require("../clients/jira/model/JiraConstants");
dayjs_1.default.extend(utc_1.default);
dayjs_1.default.extend(timezone_1.default);
dayjs_1.default.tz.setDefault("America/Los_Angeles");
class SprintService {
    constructor() {
        this.confluenceClient = new ConfluenceClient_1.ConfluenceClient();
        this.jiraClient = new JiraService_1.JiraService();
        this.alertingService = new AlertingService_1.AlertingService();
    }
    turnoverSprint(sprint) {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("SprintService:turnoverSprint", true);
            // set sprint constants
            let minutesLeft = 0;
            let sprintId = "";
            let sprintFriendlyName = "";
            switch (sprint) {
                case JiraConstants_1.Sprint.TODAY:
                    minutesLeft = (0, DateUtil_1.getMinutesLeftInADay)();
                    sprintId = JiraConstants_1.TODAY_SPRINT_ID;
                    sprintFriendlyName = "today";
                    break;
                case JiraConstants_1.Sprint.TOMORROW:
                    minutesLeft = 22 * 60;
                    sprintId = JiraConstants_1.TOMORROW_SPRINT_ID;
                    sprintFriendlyName = "tomorrow";
                    break;
                case JiraConstants_1.Sprint.NEXT_WEEK:
                    minutesLeft = 22 * 60 * 5;
                    sprintId = JiraConstants_1.NEXT_WEEK_SPRINT_ID;
                    sprintFriendlyName = "next week";
                    break;
            }
            let currentDay = (0, dayjs_1.default)();
            // move issues due today to today sprint 
            if (sprint != JiraConstants_1.Sprint.TODAY) {
                let farInThePastDay = (0, dayjs_1.default)("12-25-1995", "MM-DD-YYYY");
                let tasksDueToday = yield this.jiraClient.getSprintTasksWithDateFilter(sprintId, farInThePastDay, currentDay);
                logger.info(`tasks in ${sprintFriendlyName} due today`, tasksDueToday.length);
                yield this.jiraClient.moveIssuesToSprint(tasksDueToday, JiraConstants_1.TODAY_SPRINT_ID);
            }
            // move issues due tomorrow to tomorrow sprint
            if (sprint != JiraConstants_1.Sprint.TOMORROW) {
                let tomorrowDay = currentDay.add(1, 'day');
                let tasksDueTomorrow = yield this.jiraClient.getSprintTasksWithDateFilter(sprintId, tomorrowDay, tomorrowDay);
                logger.info(`tasks in ${sprintFriendlyName} due tomorrow`, tasksDueTomorrow.length);
                yield this.jiraClient.moveIssuesToSprint(tasksDueTomorrow, JiraConstants_1.TOMORROW_SPRINT_ID);
            }
            // move issues due next week to next sprint
            if (sprint != JiraConstants_1.Sprint.NEXT_WEEK) {
                let dayAfterTomorrowDay = currentDay.add(2, 'day');
                let endOfTheWeekDay = currentDay.add(7, 'day');
                let tasksDueInTheNextWeek = yield this.jiraClient.getSprintTasksWithDateFilter(sprintId, dayAfterTomorrowDay, endOfTheWeekDay);
                logger.info(`tasks in ${sprintFriendlyName} due in the next week`, tasksDueInTheNextWeek.length);
                yield this.jiraClient.moveIssuesToSprint(tasksDueInTheNextWeek, JiraConstants_1.NEXT_WEEK_SPRINT_ID);
            }
            // move issues due beyond next week to the backlog
            let startOfNextWeekDay = currentDay.add(8, 'day');
            let farInTheFutureDay = (0, dayjs_1.default)("12-25-2055", "MM-DD-YYYY");
            let tasksDueAfterNextWeek = yield this.jiraClient.getSprintTasksWithDateFilter(sprintId, startOfNextWeekDay, farInTheFutureDay);
            logger.info(`tasks in ${sprintFriendlyName} due after next week`, tasksDueAfterNextWeek.length);
            yield this.jiraClient.moveIssuesToBacklog(tasksDueAfterNextWeek);
            // get sprint estimates
            let tickets = yield this.jiraClient.getSprintTasks(sprintId);
            let completed = tickets.filter(ticket => ticket.status == Status_1.Status.DONE);
            let todo = tickets.filter(ticket => ticket.status != Status_1.Status.DONE);
            let currentSprintTotal = todo.map(ticket => {
                return ticket;
            })
                .reduce((sum, ticket) => sum + ticket.storyPoints, 0);
            logger.info("currentSprintTotal", currentSprintTotal);
            logger.info("minutesLeft", minutesLeft);
            let effectiveMinutesLeft = minutesLeft * 0.25;
            logger.info("effectiveMinutesLeft", effectiveMinutesLeft);
            // send alerts
            if (currentSprintTotal > minutesLeft) {
                logger.info("currentSprintTotal > minutesLeft", currentSprintTotal > minutesLeft);
                this.alertingService.sendAlert(AlertingService_1.AlertPriority.HIGH, `${currentSprintTotal} minutes of work left ${sprintFriendlyName} but only ${minutesLeft} minutes left ${sprintFriendlyName}.`);
            }
            else if (currentSprintTotal > effectiveMinutesLeft) {
                logger.info("currentSprintTotal > effectiveMinutesLeft", currentSprintTotal > effectiveMinutesLeft);
                this.alertingService.sendAlert(AlertingService_1.AlertPriority.MEDIUM, `${currentSprintTotal} minutes of work left ${sprintFriendlyName} but only ${effectiveMinutesLeft} effective minutes left ${sprintFriendlyName}`);
            }
        });
    }
    removeOldTickets() {
        return __awaiter(this, void 0, void 0, function* () {
            let currentSprintTickets = yield this.jiraClient.getCurrentSprintTasks();
            let currentSprintTicketsCompleted = currentSprintTickets.filter(ticket => ticket.status == Status_1.Status.DONE);
            this.jiraClient.moveIssuesToBacklogNonTransactional(currentSprintTicketsCompleted);
            do {
                currentSprintTickets = yield this.jiraClient.getCurrentSprintTasks();
                currentSprintTicketsCompleted = currentSprintTickets.filter(ticket => ticket.status == Status_1.Status.DONE);
            } while (currentSprintTicketsCompleted.length > 0);
        });
    }
    setDefaultStoryPoints() {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("SprintService:setDefaultStoryPoints", true);
            let tasksWithoutStoryPoints = yield this.jiraClient.getTasksWithoutStoryPoints();
            logger.info("number of tasks without story points", tasksWithoutStoryPoints.length);
            tasksWithoutStoryPoints.forEach(task => this.jiraClient.setStoryPointsForTicket(task, 10));
        });
    }
    setDefaultDueDate() {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("SprintService:setDefaultDueDate", true);
            let tasksWithoutDueDate = yield this.jiraClient.getTasksWithoutDueDate();
            logger.info("number of tasks without a due date", tasksWithoutDueDate.length);
            // let sampleTask = tasksWithoutDueDate[0]
            // this.jiraClient.setDueDateForTicket(sampleTask, dayjs())
            tasksWithoutDueDate.forEach(task => this.jiraClient.setDueDateForTicket(task, (0, dayjs_1.default)()));
        });
    }
    turnoverBacklog() {
        return __awaiter(this, void 0, void 0, function* () {
            let logger = new Logger_1.Logger("SprintService:setDefaultDueDate");
            let farInThePastDay = (0, dayjs_1.default)("12-25-1995", "MM-DD-YYYY");
            let currentDay = (0, dayjs_1.default)();
            let tomorrowDay = currentDay.add(1, 'day');
            let dayAfterTomorrowDay = currentDay.add(2, 'day');
            let endOfTheWeekDay = currentDay.add(7, 'day');
            let tasksDueTodayInBacklog = yield this.jiraClient.getTasksInBacklog(farInThePastDay, currentDay);
            logger.info("tasks in backlog due today", tasksDueTodayInBacklog.length);
            yield this.jiraClient.moveIssuesToSprint(tasksDueTodayInBacklog, JiraConstants_1.TODAY_SPRINT_ID);
            let tasksDueTomorrowInBacklog = yield this.jiraClient.getTasksInBacklog(tomorrowDay, tomorrowDay);
            logger.info("tasks in backlog due tomorrow", tasksDueTomorrowInBacklog.length);
            yield this.jiraClient.moveIssuesToSprint(tasksDueTomorrowInBacklog, JiraConstants_1.TOMORROW_SPRINT_ID);
            let tasksDueInTheNextWeekInBacklog = yield this.jiraClient.getTasksInBacklog(dayAfterTomorrowDay, endOfTheWeekDay);
            logger.info("tasks in backlog due in the next week", tasksDueInTheNextWeekInBacklog.length);
            yield this.jiraClient.moveIssuesToSprint(tasksDueInTheNextWeekInBacklog, JiraConstants_1.NEXT_WEEK_SPRINT_ID);
        });
    }
}
exports.SprintService = SprintService;
