"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ConfluenceClient_1 = require("./clients/confluence/ConfluenceClient");
const JiraService_1 = require("./services/JiraService");
const OpsgenieClient_1 = require("./clients/OpsgenieClient");
const HabitTrackingService_1 = require("./services/HabitTrackingService");
const ReflectionService_1 = require("./services/ReflectionService");
const SprintService_1 = require("./services/SprintService");
const AlertingService_1 = require("./services/AlertingService");
const Logger_1 = require("./util/Logger");
const JiraConstants_1 = require("./clients/jira/model/JiraConstants");
var opsgenie = require('opsgenie-sdk');
const app = (0, express_1.default)();
const opsgenieClient = new OpsgenieClient_1.OpsgenieClient();
const jiraClient = new JiraService_1.JiraService();
const confluenceClient = new ConfluenceClient_1.ConfluenceClient();
const habitTrackingService = new HabitTrackingService_1.HabitTrackingService();
const reflectionService = new ReflectionService_1.ReflectionService();
const sprintService = new SprintService_1.SprintService();
const alertingService = new AlertingService_1.AlertingService();
app.get('/', (request, response, next) => {
    response.send('Hello');
    var requestId = "yourRequestId";
    var create_alert_json = {
        "message": "Hello world my alert :)"
    };
});
app.get('/hello', (request, response, next) => {
    response.send('Hello');
    var requestId = "yourRequestId";
    var create_alert_json = {
        "message": "Hello world my alert :)"
    };
    //opsgenieClient.createAlert(create_alert_json)
    //jiraClient.getIssueCreateMeta()
    //confluenceClient.getAllContent()
    //jiraClient.getHabitsProjectIssues()
    //confluenceClient.getAllContent()
    //confluenceClient.getReflectionPagesIds()
    //habitTrackingService.createDailyHabits()
    //habitTrackingService.expireOldHabitTasks()
});
app.get('/publishReflectionPages', (request, response, next) => {
    reflectionService.createDailyReflectionPage();
    response.send('success');
});
app.get('/updateHabits', (request, response, next) => {
    habitTrackingService.createDailyHabits();
    habitTrackingService.expireOldHabitTasks();
    response.send('success');
});
app.get('/turnoverSprint', (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
    let logger = new Logger_1.Logger("app.ts/turnoverSprint", false);
    logger.info("/turnoverSprint request params", request.query);
    yield sprintService.turnoverSprint(JiraConstants_1.Sprint[request.query["sprint"]]);
    response.send('success');
}));
app.get('/turnoverBacklog', (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
    yield sprintService.turnoverBacklog();
    response.send('success');
}));
app.get('/setDefaultStoryPoints', (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
    yield sprintService.setDefaultStoryPoints();
    response.send('success');
}));
app.get('/setDefaultDueDate', (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
    yield sprintService.setDefaultDueDate();
    response.send('success');
}));
app.get('/sendAlert', (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
    let logger = new Logger_1.Logger("app.ts/sendAlert", false);
    logger.info(request.query["message"]);
    logger.info("/sendAlert request params", request.query);
    yield alertingService.sendAlert(AlertingService_1.AlertPriority[request.query["priority"]], request.query["message"]);
    response.send('success');
}));
app.listen(process.env.PORT || 5000, () => console.log('Server running'));
