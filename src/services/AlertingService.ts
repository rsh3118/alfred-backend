import dayjs from "dayjs"
import { ConfluenceClient } from "../clients/confluence/ConfluenceClient"
import { JiraService } from "./JiraService"
import { OpsgenieClient, OpsgenieAction} from "../clients/OpsgenieClient"

import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 
import { JiraTicket } from "../clients/jira/model/JiraTicket"
import { Task } from "../clients/jira/model/Task"
import { Status } from "../clients/jira/model/Status"
import { getMinutesLeftInADay } from "../util/DateUtil"
import { AlertConfigurationTicket } from "../clients/jira/model/AlertConfigurationTicket"
import { LOW_PRIORITY_ALERT_CONFIG_KEY, MEDIUM_PRIORITY_ALERT_CONFIG_KEY, HIGH_PRIORITY_ALERT_CONFIG_KEY } from "../clients/jira/model/JiraConstants"
import { AlertConfiguration } from "../model/AlertConfiguration"
import { TimeOfDay } from "../model/TimeOfDay"
import { Logger } from "../util/Logger"


dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")

export enum AlertPriority {
  HIGH = "HIGH",
  MEDIUM = "MEDIUM",
  LOW = "PRIORITY"
}

class DefaultAlertConfiguration implements AlertConfiguration {
  alarmStartTimeOfDay: TimeOfDay = new TimeOfDay("00:00")
  alarmEndTimeofDay: TimeOfDay = new TimeOfDay("23:59")
  shouldCallAndText: boolean = true
  shouldNotify: boolean = true
  prefix: string = 'Default'
  snoozeUntil: dayjs.Dayjs | null = null
}

export class AlertingService {
    private jiraService: JiraService = new JiraService()
    private opsgenieClient: OpsgenieClient = new OpsgenieClient()
    private lowPriorityAlertConfiguration: AlertConfiguration
    private mediumPriorityAlertConfiguration: AlertConfiguration
    private highPriorityAlertConfiguration: AlertConfiguration

    constructor(){
      this.lowPriorityAlertConfiguration = new DefaultAlertConfiguration()
      this.mediumPriorityAlertConfiguration = new DefaultAlertConfiguration()
      this.highPriorityAlertConfiguration = new DefaultAlertConfiguration()
    }

    async setAlarmConfigurations(){
      let logger = new Logger("AlertingService:setAlarmConfigurations", false)
      this.lowPriorityAlertConfiguration = await this.jiraService.getAlertConfiguration(
        LOW_PRIORITY_ALERT_CONFIG_KEY
      );
      this.mediumPriorityAlertConfiguration = await this.jiraService.getAlertConfiguration(
        MEDIUM_PRIORITY_ALERT_CONFIG_KEY
      );
      this.highPriorityAlertConfiguration = await this.jiraService.getAlertConfiguration(
        HIGH_PRIORITY_ALERT_CONFIG_KEY
      );
      logger.info("configurations loaded from jira", {
        "lowPriority": this.lowPriorityAlertConfiguration,
        "mediumPriority": this.mediumPriorityAlertConfiguration,
        "highPriority": this.highPriorityAlertConfiguration,
      })
    } 

    async sendAlert(priority: AlertPriority, message: string) {
      await this.setAlarmConfigurations();
      let logger = new Logger("AlertingService:sendAlert", false)
      let config: AlertConfiguration
      switch(priority){
        case AlertPriority.HIGH:
          config = this.highPriorityAlertConfiguration
          break
        case AlertPriority.MEDIUM:
          config = this.mediumPriorityAlertConfiguration
          break
        case AlertPriority.LOW:
          config = this.lowPriorityAlertConfiguration
          break
        default:
          throw new Error("must specify a priority")
      }
      logger.info("config", config)
      if(!this.canAlertAtCurrentTime(config)){
        return
      }
      if(config.shouldCallAndText){
        this.opsgenieClient.createAlert(
          OpsgenieAction.CALL_AND_TEXT,
          `[${config.prefix}] ${message}`
        )
      }
      if(config.shouldNotify){
        this.opsgenieClient.createAlert(
          OpsgenieAction.PUSH_NOTIFICATION,
          `[${config.prefix}] ${message}`
        )
      }
    }

    private canAlertAtCurrentTime(config: AlertConfiguration): boolean {
      let logger = new Logger("AlertingService:canAlertAtCurrentTime", false)
      logger.info("config", config)
      let currentTime = dayjs()
      logger.info("current time", currentTime)
      logger.info("current hour", currentTime.hour())
      logger.info("current minute", currentTime.minute())
      logger.info("snooze until time", config.snoozeUntil)
      logger.info("current time", currentTime)
      if(config.snoozeUntil && config.snoozeUntil > currentTime){
        logger.info("config.snoozeUntil > currentTime", config.snoozeUntil > currentTime)
        return false
      }
      logger.info("config alarm start time of day hour", config.alarmStartTimeOfDay.hour)
      logger.info("config alarm start time of day minute", config.alarmStartTimeOfDay.minute)
      logger.info("config alarm start time of day minute", ` ${config.alarmStartTimeOfDay.minute} `)
      logger.info("config alarm end time of day hour", config.alarmEndTimeofDay.hour)
      logger.info("config alarm end time of day minute", config.alarmEndTimeofDay.minute)
      let currentHour = currentTime.hour()
      let currentMinute = currentTime.minute()
      let currentMinuteTime = currentHour * 60 + currentMinute
      let startMinuteTime = config.alarmStartTimeOfDay.hour * 60 + config.alarmStartTimeOfDay.minute
      let endMinuteTime = config.alarmEndTimeofDay.hour * 60 + config.alarmEndTimeofDay.minute;
      logger.info("currentMinute", currentMinuteTime)
      logger.info("startMinuteTime", startMinuteTime)
      logger.info("endMinuteTime", endMinuteTime)
      if(startMinuteTime <= currentMinuteTime && currentMinuteTime <= endMinuteTime){
        logger.info("currentHour < config.alarmStartTimeOfDay.hour", currentHour < config.alarmStartTimeOfDay.hour)
        return true
      }
      return false
    }

    async alertOnPendingTasks(){
      await this.setAlarmConfigurations()
      let pendingTasks = await this.jiraService.getAllPendingTasks()
      pendingTasks.forEach(
        task => this.sendAlert(AlertPriority.HIGH, task.summary)
      )
    }
}