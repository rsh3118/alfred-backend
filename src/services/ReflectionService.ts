import dayjs from "dayjs"
import { ConfluenceClient } from "../clients/confluence/ConfluenceClient"
import { DailyReflectionPage } from "../clients/confluence/model/ReflectionPage"
import { JiraService } from "./JiraService"
import { OpsgenieClient } from "../clients/OpsgenieClient"
import { convertToCST } from "../util/DateUtil"

import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 

dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")

export class ReflectionService {
    confluenceClient: ConfluenceClient = new ConfluenceClient()
    jiraClient: JiraService = new JiraService()
    opsgenieClient: OpsgenieClient = new OpsgenieClient()

    constructor(){

    } 

    async createDailyReflectionPage(){
        let currentDate = dayjs().tz()
        //currentDate = convertToCST(currentDate)
        let shouldCreateTodaysReflectionPage = !(await this.hasReflectionPageAlreadyBeenPublished(currentDate))

        if(shouldCreateTodaysReflectionPage){
            let todaysReflectionPage = new DailyReflectionPage(currentDate)
            await this.confluenceClient.createReflectionPage(
                `${currentDate.format("MM/DD/YY")} Reflection`,
                todaysReflectionPage.renderXML()
            )
        }else{
        }
        
    }

    async hasReflectionPageAlreadyBeenPublished(date: dayjs.Dayjs): Promise<boolean>{
        let existingPageIds = await this.confluenceClient.getDailyReflectionPageByDate(date)
        return existingPageIds.length > 0
    }
}