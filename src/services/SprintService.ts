import dayjs from "dayjs"
import { ConfluenceClient } from "../clients/confluence/ConfluenceClient"
import { JiraService } from "./JiraService"

import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 
import { JiraTicket } from "../clients/jira/model/JiraTicket"
import { Task } from "../clients/jira/model/Task"
import { Status } from "../clients/jira/model/Status"
import { getMinutesLeftInADay } from "../util/DateUtil"
import { Logger } from "../util/Logger"
import { AlertingService, AlertPriority } from "./AlertingService"
import { COMPLETED_ID, NEXT_WEEK_SPRINT_ID, Sprint, TODAY_SPRINT_ID, TOMORROW_SPRINT_ID } from "../clients/jira/model/JiraConstants"
import { CalculateGemsResponse, EpicGemSummary, EpicGemSummaryWithTaskDetails, TaskGemSummary, TaskWithErrorDetails } from "../model/CalculateGemsResponse"
import { stringify } from "querystring"
import { Epic } from "../clients/jira/model/Epic"
import { DateTime } from "luxon"



dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")

export class SprintService {
    private confluenceClient: ConfluenceClient = new ConfluenceClient()
    private jiraService: JiraService = new JiraService()
    private alertingService: AlertingService = new AlertingService()

    constructor(){

    }

    async turnoverSprint(sprint: Sprint){
      let logger = new Logger("SprintService:turnoverSprint", true)

      // set sprint constants
      let minutesLeft = 0
      let sprintId = ""
      let sprintFriendlyName = ""
      switch (sprint){
        case Sprint.TODAY:
          minutesLeft = getMinutesLeftInADay()
          sprintId = TODAY_SPRINT_ID
          sprintFriendlyName = "today"
          break
        case Sprint.TOMORROW:
          minutesLeft = 22*60
          sprintId = TOMORROW_SPRINT_ID
          sprintFriendlyName = "tomorrow"
          break
        case Sprint.NEXT_WEEK:
          minutesLeft = 22*60*5
          sprintId = NEXT_WEEK_SPRINT_ID
          sprintFriendlyName = "next week"
          break
      }

      let currentDay = DateTime.now().setZone('America/Los_Angeles')
      let fullDate = currentDay.toISO()
      let isoDate = currentDay.toISODate()

      // move issues due today to today sprint 
      if(sprint != Sprint.TODAY){
        let farInThePastDay = DateTime.fromISO('2016-05-25T09:08:34.123', {zone: 'America/Los_Angeles'})
        let tasksDueToday = await this.jiraService.getSprintTasksWithDateTimeFilter(sprintId, farInThePastDay, currentDay)
        logger.info(`tasks in ${sprintFriendlyName} due today`, tasksDueToday.length)
        await this.jiraService.moveIssuesToSprint(tasksDueToday, TODAY_SPRINT_ID)
      }

      // move issues due tomorrow to tomorrow sprint
      if(sprint != Sprint.TOMORROW){
        let tomorrowDay = currentDay.plus({days: 1})
        let tasksDueTomorrow = await this.jiraService.getSprintTasksWithDateTimeFilter(sprintId, tomorrowDay, tomorrowDay)
        logger.info(`tasks in ${sprintFriendlyName} due tomorrow`, tasksDueTomorrow.length)
        await this.jiraService.moveIssuesToSprint(tasksDueTomorrow, TOMORROW_SPRINT_ID)
      }
      
      // move issues due next week to next sprint
      if(sprint != Sprint.NEXT_WEEK){
        let dayAfterTomorrowDay = currentDay.plus({days: 2})
        let endOfTheWeekDay = currentDay.plus({days: 7})
        let tasksDueInTheNextWeek = await this.jiraService.getSprintTasksWithDateTimeFilter(sprintId, dayAfterTomorrowDay, endOfTheWeekDay)
        logger.info(`tasks in ${sprintFriendlyName} due in the next week`, tasksDueInTheNextWeek.length)
        await this.jiraService.moveIssuesToSprint(tasksDueInTheNextWeek, NEXT_WEEK_SPRINT_ID)
      }

      // move issues due beyond next week to the backlog
      let startOfNextWeekDay = currentDay.plus({days: 8})
      let farInTheFutureDay =  DateTime.fromISO('2099-05-25T09:08:34.123', {zone: 'America/Los_Angeles'})
      let tasksDueAfterNextWeek = await this.jiraService.getSprintTasksWithDateTimeFilter(sprintId, startOfNextWeekDay, farInTheFutureDay)
      logger.info(`tasks in ${sprintFriendlyName} due after next week`, tasksDueAfterNextWeek.length)
      await this.jiraService.moveIssuesToBacklog(tasksDueAfterNextWeek)
      
      // get sprint estimates
      let tickets: Array<Task> = await this.jiraService.getSprintTasks(sprintId)
      let completed = tickets.filter(ticket => ticket.status == Status.DONE)
      let todo = tickets.filter(ticket => ticket.status != Status.DONE)
      let currentSprintTotal = todo.map(
        ticket => {
          return ticket
        }
      )
      .reduce(
        (sum, ticket) => sum + ticket.storyPoints, 0
      )
      logger.info("currentSprintTotal", currentSprintTotal)
      logger.info("minutesLeft", minutesLeft)
      let effectiveMinutesLeft = minutesLeft * 0.5
      logger.info("effectiveMinutesLeft", effectiveMinutesLeft)

      // send alerts
      // if(currentSprintTotal > minutesLeft){
      //   // logger.info("currentSprintTotal > minutesLeft", currentSprintTotal > minutesLeft)
      //   // this.alertingService.sendAlert(
      //   //   AlertPriority.HIGH,
      //   //   `${currentSprintTotal} minutes of work left ${sprintFriendlyName} but only ${minutesLeft} minutes left ${sprintFriendlyName}.`
      //   // )
      // }else if(currentSprintTotal > effectiveMinutesLeft){
      //   logger.info("currentSprintTotal > effectiveMinutesLeft", currentSprintTotal > effectiveMinutesLeft)
      //   this.alertingService.sendAlert(
      //     AlertPriority.HIGH,
      //     `${currentSprintTotal} minutes of work left ${sprintFriendlyName} but only ${effectiveMinutesLeft} effective minutes left ${sprintFriendlyName}`
      //   )
      // }
      if(currentSprintTotal > effectiveMinutesLeft){
        logger.info("currentSprintTotal > effectiveMinutesLeft", currentSprintTotal > effectiveMinutesLeft)
        this.alertingService.sendAlert(
          AlertPriority.HIGH,
          `${currentSprintTotal} minutes of work left ${sprintFriendlyName} but only ${effectiveMinutesLeft} effective minutes left ${sprintFriendlyName}`
        )
      }
    }


    private async removeOldTickets(){
      let currentSprintTickets: Array<Task> = await this.jiraService.getCurrentSprintTasks()
      let currentSprintTicketsCompleted = currentSprintTickets.filter(
        ticket => ticket.status == Status.DONE
      )
      this.jiraService.moveIssuesToBacklogNonTransactional(currentSprintTicketsCompleted)
      do {
        currentSprintTickets = await this.jiraService.getCurrentSprintTasks()
        currentSprintTicketsCompleted = currentSprintTickets.filter(
          ticket => ticket.status == Status.DONE
        )
      } while (currentSprintTicketsCompleted.length > 0)
    }

    async setDefaultStoryPoints(){
      let logger = new Logger("SprintService:setDefaultStoryPoints", false)
      let tasksWithoutStoryPoints = await this.jiraService.getTasksWithoutStoryPoints()
      logger.info("number of tasks without story points", tasksWithoutStoryPoints.length)
      tasksWithoutStoryPoints.forEach(
        task => this.jiraService.setStoryPointsForTicket(task, 10)
      )
    }

    async setDefaultDueDate(){
      let logger = new Logger("SprintService:setDefaultDueDate", false)
      let tasksWithoutDueDate = await this.jiraService.getTasksWithoutDueDate()
      logger.info("number of tasks without a due date", tasksWithoutDueDate.length)
      // let sampleTask = tasksWithoutDueDate[0]
      // this.jiraClient.setDueDateForTicket(sampleTask, dayjs())
      tasksWithoutDueDate.forEach(task => this.jiraService.setDueDateForTicket(task, dayjs()))
    }

    async turnoverBacklog(){
      let logger = new Logger("SprintService:setDefaultDueDate", false)
      let farInThePastDay = dayjs("12-25-1995", "MM-DD-YYYY")
      let currentDay = dayjs()
      let tomorrowDay = currentDay.add(1, 'day')
      let dayAfterTomorrowDay = currentDay.add(2, 'day')
      let endOfTheWeekDay = currentDay.add(7, 'day')
      let tasksDueTodayInBacklog = await this.jiraService.getTasksInBacklog(farInThePastDay, currentDay)
      logger.info("tasks in backlog due today", tasksDueTodayInBacklog.length)
      await this.jiraService.moveIssuesToSprint(tasksDueTodayInBacklog, TODAY_SPRINT_ID)
      let tasksDueTomorrowInBacklog = await this.jiraService.getTasksInBacklog(tomorrowDay, tomorrowDay)
      logger.info("tasks in backlog due tomorrow", tasksDueTomorrowInBacklog.length)
      await this.jiraService.moveIssuesToSprint(tasksDueTomorrowInBacklog, TOMORROW_SPRINT_ID)
      let tasksDueInTheNextWeekInBacklog = await this.jiraService.getTasksInBacklog(dayAfterTomorrowDay, endOfTheWeekDay)
      logger.info("tasks in backlog due in the next week", tasksDueInTheNextWeekInBacklog.length)
      await this.jiraService.moveIssuesToSprint(tasksDueInTheNextWeekInBacklog, NEXT_WEEK_SPRINT_ID)
    }

    async calculateGemValues(): Promise<CalculateGemsResponse> {
      let logger = new Logger("SprintService:calculateGemValues", false)
      let tasks = await this.jiraService.getSprintTasks(COMPLETED_ID)
      let parentEpicKeys = tasks.filter(
        task => task.parent !== null
      ).map(
        task => task.parent?.key
      )
      let parentEpics = await this.jiraService.getEpics(
        parentEpicKeys as Array<string>
      )
      let epicMap = parentEpics.reduce(function(map, epic) {
        map.set(epic.key, epic);
        return map;
      }, new Map<string, Epic>());
      let completedTasks = tasks.filter(
        task => task.status == Status.DONE
      )
      let validTasks = completedTasks.filter(
        task => this.isValidCompletedTask(task, epicMap)
      )
      let invalidTasks = completedTasks.filter(
        task => !this.isValidCompletedTask(task, epicMap)
      )
      validTasks.forEach(
        task => this.jiraService.updateStoryPoints(task.key, this.calculateGems(task, epicMap))
      )
      let detailedSummaries = [
        ...
        validTasks
        .filter(task => task.parent != null)
        .map(task => new TaskGemSummary(
          task.key,
          task.summary,
          task.parent?.summary as string,
          this.calculateGems(task, epicMap),
          task.noveltyMultiplier as number,
          task.inertiaMultiplier as number,
          task.importanceMultiplier as number,
          task.prideMultiplier as number,
          task.speedMultiplier as number,
          epicMap.get(task.parent?.key as string)?.multiplier as number,
          task.storyPoints
        ))
        .reduce(function(map, taskGemSummary) {
          let epic = taskGemSummary.parentSummary
          let gemsEarned = taskGemSummary.gemsEarned
          if(!map.has(epic)){
            map.set(epic, new EpicGemSummaryWithTaskDetails(epic, 0, []))
          }
          let summary: EpicGemSummaryWithTaskDetails = map.get(epic) as EpicGemSummaryWithTaskDetails
          summary.epicGemTotal = summary.epicGemTotal + gemsEarned
          summary.taskGemSummaries.push(taskGemSummary)
          return map;
        }, new Map<string, EpicGemSummaryWithTaskDetails>()).values()
      ]
      return new CalculateGemsResponse(
        invalidTasks.map(task => this.getTaskError(task, epicMap)),
        detailedSummaries.map(detailedSummary => new EpicGemSummary(
          detailedSummary.epicName,
          detailedSummary.epicGemTotal
        )),
        detailedSummaries
      )
    }

    isValidCompletedTask(task: Task, epicMap: Map<string, Epic>): boolean {
      let logger = new Logger("SprintService:isValidCompletedTask", false)
      let nullMultipliers = [
        task.importanceMultiplier,
        task.inertiaMultiplier,
        task.noveltyMultiplier,
        task.prideMultiplier,
        task.speedMultiplier
      ].filter(
        multiplier => multiplier === null
      )
      logger.info("task details", {
        multipliers: [
          task.importanceMultiplier,
          task.inertiaMultiplier,
          task.noveltyMultiplier,
          task.prideMultiplier,
          task.speedMultiplier
        ],
        nullMultipliers,
        parent: task.parent
      })
      
      return nullMultipliers.length == 0 && task.parent !== null && epicMap.has(task.parent.key)
    }

    getTaskError(task: Task, epicMap: Map<string, Epic>): TaskWithErrorDetails {
      let logger = new Logger("SprintService:getTaskError", false)
      let errors = [
        {label: "importance multiplier", value: task.importanceMultiplier},
        {label: "importance multiplier", value: task.inertiaMultiplier},
        {label: "importance multiplier", value: task.noveltyMultiplier},
        {label: "importance multiplier", value: task.prideMultiplier},
        {label: "importance multiplier", value: task.speedMultiplier},
        {label: "story point", value: task.storyPoints},
        {label: "parent", value: task.parent ? task.parent.summary : null}
      ].filter(
        field => field.value === null
      ).map(
        field => `${field.label} is ${field.value}`
      )
      return new TaskWithErrorDetails(
        task.key,
        task.summary,
        task.parent ? task.parent.summary : null,
        errors
      )
    }

    calculateGems(task: Task, epicMap: Map<string, Epic>): number {
      let epicMultiplier = epicMap.get(task.parent?.key as string)?.multiplier as number
      let gemsEarned = [
        task.importanceMultiplier,
        task.inertiaMultiplier,
        task.noveltyMultiplier,
        task.prideMultiplier,
        task.speedMultiplier
      ].filter(
        multiplier => multiplier != null
      ).reduce(
        (currentGemValue, multiplier) => (currentGemValue as number) * (multiplier as number), task.storyPoints * epicMultiplier
      )
      return Math.ceil(gemsEarned as number)
    }
}