import dayjs, { Dayjs } from "dayjs";
import { HabitEpic } from "../clients/jira/model/HabitEpic";
import { JiraSearchResponse } from "../clients/jira/model/JiraSearchResponse";
import { JiraTicket } from "../clients/jira/model/JiraTicket";
import { Status } from "../clients/jira/model/Status";
import { Task } from "../clients/jira/model/Task";

var JiraApi = require('jira-client');

// This code sample uses the 'node-fetch' library:
// https://www.npmjs.com/package/node-fetch
const fetch = require('node-fetch');

import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 
import { AlertConfigurationTicket } from "../clients/jira/model/AlertConfigurationTicket";
import { Logger } from "../util/Logger";
import { JiraClient } from "../clients/jira/JiraClient";
import { Epic } from "../clients/jira/model/Epic";
import { FAILED_TRANSITION, GEMS_EARNED_FIELD } from "../clients/jira/model/JiraConstants";
import { DateTime } from "luxon";

dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")

export class JiraService {

  private jiraClient = new JiraClient()

  constructor(){
      // Initialize
      var jira = new JiraApi({
          protocol: 'https',
          host: 'ritwik.atlassian.net',
          username: 'rsh3118@gmail.com',
          password: 'l4IcH8348aOwLNg05CKi0F5D',
          apiVersion: '2',
          strictSSL: true
      });
  }
  
  /**
   * Returns the meta data for creating issues. This includes the available projects, issue types and fields, including field types and whether or not those fields are required.
   * Projects will not be returned if the user does not have permission to create issues in that project.
   * The fields in the createmeta correspond to the fields in the create screen for the project/issuetype. Fields not in the screen will not be in the createmeta.
   * Fields will only be returned if expand=projects.issuetypes.fields.
   * The results can be filtered by project and/or issue type, given by the query params.
   */
  async getIssueCreateMeta(){
      var url = new URL('https://ritwik.atlassian.net/rest/api/3/issue/createmeta')
      var params = {expand: 'projects.issuetypes.fields'}
      url.search = new URLSearchParams(params).toString();
        var response: Response = await fetch(url, {
          method: 'GET',
          headers: {
            'Authorization': `Basic ${Buffer.from(
              'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
            ).toString('base64')}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
        })
        try {
            let text: string = await response.text()
        } catch (err) {
          console.log(err)
        }
  }

  async setStoryPointsForTicket(ticket: JiraTicket, storyPoints: number){
    let logger = new Logger("JiraClient:setStoryPointsForTicket", false)
    logger.info("ticket key", ticket.key)
    logger.info("story points", storyPoints)
    const bodyData = `{
      "fields": { 
          "Story Points" : ${storyPoints}
      }
    }`;
    var url = new URL(`https://ritwik.atlassian.net/rest/api/3/issue/${ticket.key}`)
    var response: Response = await fetch(url, {
    method: 'PUT',
    headers: {
        'Authorization': `Basic ${Buffer.from(
        'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
        ).toString('base64')}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: bodyData
    })
    try {
      //logger.info(`Response: ${response.status} ${response.statusText}`);
      let res = await response.json()
      console.log(res)
    } catch (err) {
      //logger.error("error", err)
      throw err
    }
  }

  async setDueDateForTicket(ticket: JiraTicket, dueDate: Dayjs){
    let logger = new Logger("JiraClient:setStoryPointsForTicket", false)
    logger.info("ticket key", ticket.key)
    let todaysDate = `${dueDate.format('YYYY-MM-DD')}`
    logger.info("todays date", todaysDate)
    const bodyData = `{
      "fields":{
        "duedate" : "${todaysDate}"
      }
    }`;
    var url = new URL(`https://ritwik.atlassian.net/rest/api/3/issue/${ticket.key}`)
    var response: Response = await fetch(url, {
    method: 'PUT',
    headers: {
        'Authorization': `Basic ${Buffer.from(
        'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
        ).toString('base64')}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: bodyData
    })
    try {
      //logger.info(`Response: ${response.status} ${response.statusText}`);
      //let res = await response.json()
      //console.log(res)
    } catch (err) {
      //logger.error("error", err)
      throw err
    }
  }

  async getCurrentSprintTasks(): Promise<Array<Task>> {
    var url = new URL('https://ritwik.atlassian.net/rest/agile/1.0/sprint/25/issue')
    var params = {expand: 'names'}
    url.search = new URLSearchParams(params).toString();
      var response: Response = await fetch(url, {
        method: 'GET',
        headers: {
          'Authorization': `Basic ${Buffer.from(
            'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
          ).toString('base64')}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      try {
        let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
        return searchResponse.issues
      } catch (err) {
        console.log(err)
        throw err;
      }
  }

  async getAlertConfiguration(key: String): Promise<AlertConfigurationTicket> {
    var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
    var params = {
      jql: `project = CONFIG AND key="${key}"`, 
      expand: 'names,renderedFields'
    }
    url.search = new URLSearchParams(params).toString();
    var response: Response = await fetch(url, {
    method: 'GET',
    headers: {
        'Authorization': `Basic ${Buffer.from(
        'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
        ).toString('base64')}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    })
    let responseBody = await response.json()
    try {
        let searchResponse: JiraSearchResponse<AlertConfigurationTicket> = new JiraSearchResponse(responseBody, AlertConfigurationTicket)
        return searchResponse.issues[0]
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  async getSprints() {
    var url = new URL('https://ritwik.atlassian.net/rest/agile/1.0/board/1/sprint')
    var params = {expand: 'names'}
    url.search = new URLSearchParams(params).toString();
      var response: Response = await fetch(url, {
        method: 'GET',
        headers: {
          'Authorization': `Basic ${Buffer.from(
            'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
          ).toString('base64')}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
  }

  async getHabitsProjectIssues(){
      var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
      var params = {jql: 'project=Habits'}
      url.search = new URLSearchParams(params).toString();
      var response: Response = await fetch(url, {
      method: 'GET',
      headers: {
          'Authorization': `Basic ${Buffer.from(
          'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
          ).toString('base64')}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }
      })
      try {
          let text: string = await response.text()
      } catch (err) {
      console.log(err)
      }
  }

  async getHabitsProjectsEpics(): Promise<Array<HabitEpic>>{
    var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
      var params = {
        jql: 'project=Habits and type = Epic and status = "To do"', 
        expand: 'names,renderedFields'
      }
      url.search = new URLSearchParams(params).toString();
      var response: Response = await fetch(url, {
      method: 'GET',
      headers: {
          'Authorization': `Basic ${Buffer.from(
          'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
          ).toString('base64')}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }
      })

      try {
          let searchResponse: JiraSearchResponse<HabitEpic> = new JiraSearchResponse(await response.json(), HabitEpic)
          return searchResponse.issues
      } catch (err) {
        console.log(err)
        throw err
      }
  }

  async getTasksForHabitEpic(habitEpic: HabitEpic, date: dayjs.Dayjs): Promise<Array<Task>>{
    var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
      var params = {
        jql: `project=Habits and parent in ("${habitEpic.key}")`, 
        expand: 'names,renderedFields'
      }
      url.search = new URLSearchParams(params).toString();
      var response: Response = await fetch(url, {
      method: 'GET',
      headers: {
          'Authorization': `Basic ${Buffer.from(
          'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
          ).toString('base64')}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      })
      try {
          let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
          return searchResponse.issues
      } catch (err) {
          console.log(err)
          throw err
      }
  }

  async getTasksWithoutStoryPoints(): Promise<Array<Task>>{
    var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
      var params = {
        jql: `project = Tasks AND "Story Points" is empty`, 
        expand: 'names,renderedFields'
      }
      url.search = new URLSearchParams(params).toString();
      var response: Response = await fetch(url, {
      method: 'GET',
      headers: {
          'Authorization': `Basic ${Buffer.from(
          'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
          ).toString('base64')}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      })
      try {
          let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
          return searchResponse.issues
      } catch (err) {
          console.log(err)
          throw err
      }
  }

  async getTasksWithoutDueDate(): Promise<Array<Task>>{
    var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
      var params = {
        jql: `project = Tasks AND duedate is empty AND issuetype = TASK`, 
        expand: 'names,renderedFields'
      }
      url.search = new URLSearchParams(params).toString();
      var response: Response = await fetch(url, {
      method: 'GET',
      headers: {
          'Authorization': `Basic ${Buffer.from(
          'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
          ).toString('base64')}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      })
      try {
          let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
          return searchResponse.issues
      } catch (err) {
          console.log(err)
          throw err
      }
  }

  async getTasksInBacklog(startDate: Dayjs, endDate: Dayjs){
    return await this.jiraClient.searchIssues(`(Sprint is empty) AND issuetype = TASK AND project = Tasks AND (due >= ${startDate.format('YYYY-MM-DD')} AND due <= ${endDate.format('YYYY-MM-DD')}) AND status != Done AND status != Failed`)

    // var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
    //   var params = {
    //     jql: `(Sprint is empty) AND issuetype = TASK AND project = Tasks AND (due >= ${startDate.format('YYYY-MM-DD')} AND due <= ${endDate.format('YYYY-MM-DD')}) AND status != Done AND status != Failed`, 
    //     expand: 'names,renderedFields'
    //   }
    //   url.search = new URLSearchParams(params).toString();
    //   var response: Response = await fetch(url, {
    //   method: 'GET',
    //   headers: {
    //       'Authorization': `Basic ${Buffer.from(
    //       'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
    //       ).toString('base64')}`,
    //       'Accept': 'application/json',
    //       'Content-Type': 'application/json'
    //   },
    //   })
    //   try {
    //       let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
    //       return searchResponse.issues
    //   } catch (err) {
    //       console.log(err)
    //       throw err
    //   }
  }


  async createHabitTask(parentKey: string, summary: string, dueDate: string){
    let bodyData = `{
      "update": {},
      "fields": {
        "summary": "${summary}",
        "parent": {
          "key": "${parentKey}"
        },
        "issuetype": {
          "id": "10016"
        },
        "project": {
          "id": "10005"
        },
        "reporter": {
          "id": "557058:2f12732f-687c-4396-9b05-c1fe99179612"
        },
        "duedate": "${dueDate}",
        "assignee": {
          "id": "557058:2f12732f-687c-4396-9b05-c1fe99179612"
        }
      }
    }`;
    var url = new URL('https://ritwik.atlassian.net/rest/api/3/issue')
    var response: Response = await fetch(url, {
    method: 'POST',
    headers: {
        'Authorization': `Basic ${Buffer.from(
        'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
        ).toString('base64')}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: bodyData
    })
    try {
        let res = await response.json()
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  async getTransitions(taskKey: string){
    var url = new URL(`https://ritwik.atlassian.net/rest/api/3/issue/${taskKey}/transitions`)
    var response: Response = await fetch(url, {
    method: 'GET',
    headers: {
        'Authorization': `Basic ${Buffer.from(
        'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
        ).toString('base64')}`,
        'Accept': 'application/json',
    },
    })
    try {
        let res = await response.json()
        console.log(res)
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  async setHabitTaskStatusToFailed(taskKey: string){
    const bodyData = `{
      "update": {
        "comment": [
          {
            "add": {
              "body": {
                "type": "doc",
                "version": 1,
                "content": [
                  {
                    "type": "paragraph",
                    "content": [
                      {
                        "text": "Issue has expired and has not been completed marking it as failed",
                        "type": "text"
                      }
                    ]
                  }
                ]
              }
            }
          }
        ]
      },
      "transition": {
        "id": "21"
      }
    }`;
    var url = new URL(`https://ritwik.atlassian.net/rest/api/3/issue/${taskKey}/transitions`)
    var response: Response = await fetch(url, {
    method: 'POST',
    headers: {
        'Authorization': `Basic ${Buffer.from(
        'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
        ).toString('base64')}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: bodyData
    })
    try {
        let res = await response.json()
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  async moveIssuesToBacklogNonTransactional(backlogTasks: Task[]) {
    const bodyData = `{
      "issues": [${backlogTasks.map(task => `"${task.key}"`).join(",")}]
    }`;
    var url = new URL(`https://ritwik.atlassian.net/rest/agile/1.0/backlog/issue`)
    try {
        let requestDetails = {
          method: 'POST',
          headers: {
              'Authorization': `Basic ${Buffer.from(
              'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
              ).toString('base64')}`,
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          body: bodyData
          }
        var response: Response = await fetch(`https://ritwik.atlassian.net/rest/agile/1.0/backlog/issue`, {...requestDetails})
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  async moveIssuesToSprintNonTransactional(backlogTasks: Task[], sprintId: string) {
    let logger = new Logger("JiraClient:moveIssuesToSprint", false)
    logger.info("sprintId", sprintId)
    const bodyData = `{
      "issues": [${backlogTasks.map(task => `"${task.key}"`).join(",")}]
    }`;
    var url = new URL(`https://ritwik.atlassian.net/rest/agile/1.0/sprint/${sprintId}/issue`)
    logger.info("url", url)
    try {
        let requestDetails = {
          method: 'POST',
          headers: {
              'Authorization': `Basic ${Buffer.from(
              'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
              ).toString('base64')}`,
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          body: bodyData
          }
        var response: Response = await fetch(url, {...requestDetails})
        logger.info("response", `Response: ${response.status} ${response.statusText}`)
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  // async moveIssuesToSprintNonTransactional(backlogTasks: Task[], sprintId: string) {
  //   let logger = new Logger("JiraClient:moveIssuesToSprint", true)
  //   logger.info("sprintId", sprintId)
  //   const bodyData = `{
  //     "issues": [${backlogTasks.map(task => `"${task.key}"`).join(",")}]
  //   }`;
  //   var url = new URL(`https://ritwik.atlassian.net/rest/agile/1.0/sprint/${sprintId}/issue`)
  //   logger.info("url", url)
  //   try {
  //       let requestDetails = {
  //         method: 'POST',
  //         headers: {
  //             'Authorization': `Basic ${Buffer.from(
  //             'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
  //             ).toString('base64')}`,
  //             'Accept': 'application/json',
  //             'Content-Type': 'application/json'
  //         },
  //         body: bodyData
  //         }
  //       var response: Response = await fetch(url, {...requestDetails})
  //       logger.info("response", `Response: ${response.status} ${response.statusText}`)
  //   } catch (err) {
  //       console.log(err)
  //       throw err
  //   }
  // }

  async moveIssuesToBacklog(tasks: Task[]){
    await this.moveIssuesToBacklogNonTransactional(tasks)
    var setOfTaskKeysMoved = new Set(tasks.map(task => task.key));
    let farInThePastDay = dayjs("12-25-1995", "MM-DD-YYYY")
    let farInTheFutureDay = dayjs("12-25-2055", "MM-DD-YYYY")
    let setOfTaskKeysInSprint = new Set((await this.getTasksInBacklog(farInThePastDay, farInTheFutureDay)).map(task => task.key))
    let allTasksAreInSprint = true
    do{
      await Promise.all(
        [...setOfTaskKeysMoved].map(async taskKey => {
          allTasksAreInSprint = allTasksAreInSprint && setOfTaskKeysInSprint.has(taskKey)
          setOfTaskKeysInSprint = new Set((await this.getTasksInBacklog(farInThePastDay, farInTheFutureDay)).map(task => task.key))
        })
      )
    }while(!allTasksAreInSprint)
  }

  //  methods using jira client 
  async getSprintTasks(sprintId: string): Promise<Array<Task>>{
    return await this.jiraClient.searchIssues(`Sprint = ${sprintId}`)
  }

  async getEpics(epicKeys: Array<string>): Promise<Array<Epic>>{
    return await this.jiraClient.searchEpics(`key in (${epicKeys.join(", ")})`)
  }

  async getSprintTasksWithDateFilter(sprintId: string, startDate: Dayjs, endDate: Dayjs){
    return await this.jiraClient.searchIssues(`Sprint = ${sprintId} AND (due >= ${startDate.format('YYYY-MM-DD')} AND due <= ${endDate.format('YYYY-MM-DD')})`)
  }

  async getSprintTasksWithDateTimeFilter(sprintId: string, startDate: DateTime, endDate: DateTime){
    return await this.jiraClient.searchIssues(`Sprint = ${sprintId} AND (due >= ${startDate.toISODate()} AND due <= ${endDate.toISODate()})`)
  }

  async getEfficiencyConfiguration(key: string): Promise<JiraTicket> {
    return (await this.jiraClient.searchIssues(`project = CONFIG AND key="${key}"`))[0]
  }

  async findTasks(summary: string, status: string): Promise<Array<JiraTicket>> {
    return  (await this.jiraClient.searchIssues(`project = TASK AND summary ~ "${summary}" AND status = "${status}"`))
  }

  async transitionIssueToFailed(key: string){
    await this.jiraClient.transitionIssue(key, FAILED_TRANSITION)
  }

  async createTask(parentKey: string, summary: string, dueDate: string, alarmTime: string) {
    return await this.jiraClient.createTask(parentKey, summary, dueDate, alarmTime)
  }
  
  async moveIssuesToSprint(tasksToRemove: Task[], sprintId: string){
    let logger = new Logger("JiraService:moveIssuesToSprint", false)
    let currentIndex = 0
    let size = 25
    let total = tasksToRemove.length
    let tasks = tasksToRemove.slice(currentIndex, currentIndex + size)
    do {
      let tasks = tasksToRemove.slice(currentIndex, currentIndex + size)
      logger.info("pagination status", {
        currentIndex,
        size,
        total
      })
      await this.jiraClient.addIssuesToSprint(tasks, sprintId)
      var setOfTaskKeysMoved = new Set(tasks.map(task => task.key));
      let setOfTaskKeysInSprint = new Set((await this.getSprintTasks(sprintId)).map(task => task.key))
      let allTasksAreInSprint = true
      do{
        await Promise.all(
          [...setOfTaskKeysMoved].map(async taskKey => {
            allTasksAreInSprint = allTasksAreInSprint && setOfTaskKeysInSprint.has(taskKey)
            setOfTaskKeysInSprint = new Set((await this.getSprintTasks(sprintId)).map(task => task.key))
          })
        )
      }while(!allTasksAreInSprint)
      currentIndex = currentIndex + size
    }while(currentIndex < total)
  }

  async updateStoryPoints(key: string, storyPoints: number){
    await this.jiraClient.updateField(key, GEMS_EARNED_FIELD, `${storyPoints}`)
  }

  async getAllPendingTasks(): Promise<Array<Task>>{
    return await this.jiraClient.searchIssues(`project = TASK AND status = "To Do" AND "Alarm Time[Time stamp]" <= now()`)
  }
}