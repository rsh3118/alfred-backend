import dayjs from "dayjs";
import { ConfluenceClient } from "../clients/confluence/ConfluenceClient";
import { JiraService } from "./JiraService";
import { Cadence, DailyCadence, DayOfTheMonthCadence, DayOfWeekCadence, EverydayExceptFridayCadence, EveryXDaysCadence, LastDayOfTheMonthCadence, MonthlyCadence, WeekdayCadence, WeekendCadence, WeeklyCadence } from "../clients/jira/model/Cadence";
import { Status } from "../clients/jira/model/Status";
import { OpsgenieClient } from "../clients/OpsgenieClient";
import { convertToCST, isEndOfMonth, isFriday, isSameDay, isSameMonth, isSameWeek, isWeekday, isWeekend } from "../util/DateUtil";
import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 
import { TO_DO_STATUS } from "../clients/jira/model/JiraConstants";
import { Logger } from "../util/Logger";
import { DateTime } from "luxon";

dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")

export class HabitTrackingService {
    confluenceClient: ConfluenceClient = new ConfluenceClient()
    jiraService: JiraService = new JiraService()
    opsgenieClient: OpsgenieClient = new OpsgenieClient()

    constructor(){

    } 

    async createDailyHabits(){
        let currentDate: dayjs.Dayjs  = dayjs().tz()
        let habitEpics = await this.jiraService.getHabitsProjectsEpics()
        habitEpics.forEach(async habitEpic => {
            let habitTasks = await (await this.jiraService.getTasksForHabitEpic(habitEpic, dayjs().tz()))
            if(habitTasks.length > 0){
                habitTasks.sort((a,b) => a.dueDate.valueOf() - b.dueDate.valueOf())
                let mostRecentTask = habitTasks[habitTasks.length - 1]
                if(this.shouldNewTaskBeCreated(habitEpic.cadence, mostRecentTask.dueDate, currentDate)){
                    this.jiraService.createHabitTask(
                        habitEpic.key,
                        `${habitEpic.summary} ${currentDate.format('MM/DD/YY')}`,
                        `${currentDate.format("YYYY-MM-DD")}`
                    )
                }else{
                }
            }else{
                this.jiraService.createHabitTask(
                    habitEpic.key,
                    `${habitEpic.summary} ${currentDate.format('MM/DD/YY')}`,
                    `${currentDate.format("YYYY-MM-DD")}`
                )
            }  
        })
    }

    async expireOldHabitTasks(){
        let currentDate: dayjs.Dayjs  = dayjs().tz()
        let habitEpics = await this.jiraService.getHabitsProjectsEpics()
        habitEpics.forEach(async habitEpic => {
            let habitTasks = await (await this.jiraService.getTasksForHabitEpic(habitEpic, dayjs().tz()))
            habitTasks.forEach(task => {
                if(task.status == Status.TODO && this.isHabitTaskExpired(habitEpic.cadence, task.dueDate, currentDate)){
                    this.jiraService.setHabitTaskStatusToFailed(task.key)
                }
            })
        })
    }

    isHabitTaskExpired(cadence: Cadence, lastTaskDueDate: dayjs.Dayjs, currentDate: dayjs.Dayjs): boolean{
        if(cadence instanceof DailyCadence){
            let cutoff = lastTaskDueDate.add(1, 'day')
            let res = cutoff.valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof WeeklyCadence){
            let res = lastTaskDueDate.add(3, 'day').add(12, 'hour').valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof MonthlyCadence){
            let res = lastTaskDueDate.add(15, 'day').valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof EveryXDaysCadence){
            let res = lastTaskDueDate.add(cadence.x * 0.5, 'day').valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof DayOfWeekCadence){
            let res = lastTaskDueDate.add(3, 'day').add(12, 'hour').valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof DayOfTheMonthCadence){ 
            let res = lastTaskDueDate.add(15, 'day').valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof LastDayOfTheMonthCadence){
            let res = lastTaskDueDate.add(15, 'day').valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof WeekdayCadence){
            let res = lastTaskDueDate.add(1, 'day').add(12, 'hour').valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof WeekendCadence){
            let res = lastTaskDueDate.add(1, 'day').add(12, 'hour').valueOf() < currentDate.valueOf()
            return res
        }
        if(cadence instanceof EverydayExceptFridayCadence){
            let res = lastTaskDueDate.add(1, 'day').add(12, 'hour').valueOf() < currentDate.valueOf()
            return res
        }
        throw new Error("should be impossible to reach")
    }

    shouldNewTaskBeCreated(cadence: Cadence, lastTaskDueDate: dayjs.Dayjs, currentDate: dayjs.Dayjs): boolean{
        if(cadence instanceof DailyCadence){
            let res = !isSameDay(lastTaskDueDate, currentDate)
            return res
        }
        if(cadence instanceof WeeklyCadence){
            let res = !isSameWeek(lastTaskDueDate, currentDate)
            return res
        }
        if(cadence instanceof MonthlyCadence){
            let res = !isSameMonth(lastTaskDueDate, currentDate)
            return res
        }
        if(cadence instanceof EveryXDaysCadence){
            let res = lastTaskDueDate.startOf('day').add(cadence.x, 'day').subtract(1, 'second').valueOf() < currentDate.startOf('day').valueOf()
            return res
        }
        if(cadence instanceof DayOfWeekCadence){
            let res = !isSameDay(lastTaskDueDate, currentDate) && currentDate.day() === cadence.getDayOfWeekNumber()
            return res
        }
        if(cadence instanceof DayOfTheMonthCadence){ 
            let res = !isSameDay(lastTaskDueDate, currentDate) && Number(currentDate.format('DD')) === Number(cadence.dayOfMonth.toString())
            return res
        }
        if(cadence instanceof LastDayOfTheMonthCadence){
            let res = !isSameDay(lastTaskDueDate, currentDate) && isEndOfMonth(currentDate)
            return res
        }
        if(cadence instanceof WeekdayCadence){
            let res = !isSameDay(lastTaskDueDate, currentDate) && isWeekday(currentDate)
            return res
        }
        if(cadence instanceof WeekendCadence){
            let res = !isSameDay(lastTaskDueDate, currentDate) && isWeekend(currentDate)
            return res
        }
        if(cadence instanceof EverydayExceptFridayCadence){
            let res = !isSameDay(lastTaskDueDate, currentDate) && !isFriday(currentDate)
            return res
        }
        throw new Error("should be impossible to reach")
    }

    async createHabit(parentKey: string, summary: string, minutesToComplete: number){
        let logger = new Logger("HabitTrackingService:createHabit", true)
        // find existing task
        let existingTasks = await this.jiraService.findTasks(summary, TO_DO_STATUS)

        // fail existing task if it exists
        existingTasks.forEach(task => this.jiraService.transitionIssueToFailed(task.key))

        // create new task
        let date = DateTime.now().setZone('America/Los_Angeles').plus({minutes: minutesToComplete})
        let fullDate = date.toISO()
        let isoDate = date.toISODate()
        
        let currentDay = dayjs(new Date()).utc(true)
        let dueTime = currentDay.add(minutesToComplete, 'minute')
        let dueDate = dueTime.utc(true).format("YYYY-MM-DD")
        let alarmTime = dueTime.utc(true).format("YYYY-MM-DDTHH:mm:ss")
        logger.info("create params", {parentKey, summary, dueDate, alarmTime, fullDate, isoDate})
        await this.jiraService.createTask(parentKey, summary, isoDate, fullDate)
        return
    }
}