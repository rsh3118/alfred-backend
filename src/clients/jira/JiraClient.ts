import { Logger } from "../../util/Logger";
import {HttpMethod } from "../RestAPIConstants";
import { Epic } from "./model/Epic";
import { ALARM_TIME_CUSTOM_FIELD, TASK_ISSUE_TYPE, TASK_PROJECT_ID } from "./model/JiraConstants";
import { JiraSearchResponse } from "./model/JiraSearchResponse";
import { Task } from "./model/Task";
const fetch = require('node-fetch');

export class JiraClient {
  baseUrl = `https://ritwik.atlassian.net`
  searchEndpoint = `/rest/api/3/search`

  async callJiraApi(method: HttpMethod, endpoint: string, params: any, body: any): Promise<any> {
    var url = new URL(`${this.baseUrl}${endpoint}`)
    if(params){
      url.search = new URLSearchParams(params).toString();
    }
    let requestDetails: any = {
      method: method,
      headers: {
          'Authorization': `Basic ${Buffer.from(
          'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
          ).toString('base64')}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
    }
    if(body){
      requestDetails = {...requestDetails, body}
    }
    var response: Response = await fetch(url, requestDetails)
    return response
  }

  private async searchIssuesWithoutPagination(filter: string, startIndex: number): Promise<JiraSearchResponse<Task>>{
    let logger = new Logger("JiraClient:searchIssuesWithoutPagination", false)
    var params = {
      jql: filter, 
      expand: 'names,renderedFields',
      startAt: startIndex
    }
    let response = await this.callJiraApi(HttpMethod.GET, this.searchEndpoint, params, null)
    try {
      let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
      return searchResponse
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  private async searchEpicsWithoutPagination(filter: string, startIndex: number): Promise<JiraSearchResponse<Epic>>{
    let logger = new Logger("JiraClient:searchIssuesWithoutPagination", false)
    var params = {
      jql: filter, 
      expand: 'names,renderedFields',
      startAt: startIndex
    }
    let response = await this.callJiraApi(HttpMethod.GET, this.searchEndpoint, params, null)
    try {
      let searchResponse: JiraSearchResponse<Epic> = new JiraSearchResponse(await response.json(), Epic)
      return searchResponse
    } catch (err) {
        console.log(err)
        throw err
    }
  }


  async searchIssues(filter: string){
    let logger = new Logger("JiraClient:searchIssues", false)
    let tasks: Array<Task> = []
    let currentIndex = 0
    let totalResults = 0
    let maxResults = 0
    do {
      let response = await this.searchIssuesWithoutPagination(filter, currentIndex)
      logger.info("issues", response.issues.map(issue => issue.key))
      currentIndex = response.startAt
      totalResults = response.total
      maxResults = response.maxResults
      tasks.push(...response.issues)
      logger.info("pagination status", {
        filter,
        currentIndex,
        totalResults,
        maxResults
      })
      currentIndex = currentIndex + response.issues.length
    } while (currentIndex < totalResults - 1)
    return tasks
  }

  async searchEpics(filter: string){
    let logger = new Logger("JiraClient:searchIssues", false)
    let tasks: Array<Epic> = []
    let currentIndex = 0
    let totalResults = 0
    let maxResults = 0
    do {
      let response = await this.searchEpicsWithoutPagination(filter, currentIndex)
      logger.info("issues", response.issues.map(issue => issue.key))
      currentIndex = response.startAt
      totalResults = response.total
      maxResults = response.maxResults
      tasks.push(...response.issues)
      logger.info("pagination status", {
        filter,
        currentIndex,
        totalResults,
        maxResults
      })
      currentIndex = currentIndex + response.issues.length
    } while (currentIndex < totalResults - 1)
    return tasks
  }

  async addIssuesToSprint(backlogTasks: Task[], sprintId: string) {
    let logger = new Logger("JiraClient:addIssuesToSprint", false)
    logger.info("sprintId", sprintId)
    const body = `{
      "issues": [${backlogTasks.map(task => `"${task.key}"`).join(",")}]
    }`;
    await this.callJiraApi(
      HttpMethod.POST,
      `/rest/agile/1.0/sprint/${sprintId}/issue`,
      {},
      body
    )
  }

  async getIssueMetadata(key: string): Promise<any> {
    let logger = new Logger("JiraClient:getIssueMetadata", false)
    let response = await this.callJiraApi(HttpMethod.GET, `/rest/api/3/issue/${key}/editmeta`, null, null)
    try {
      let issueMetadataResponse: any = await response.json()
      logger.info("get issue metadata response", issueMetadataResponse)
      return issueMetadataResponse;
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  async getIssueTransitions(key: string): Promise<any> {
    let logger = new Logger("JiraClient:getIssueTransitions", false)
    let response = await this.callJiraApi(HttpMethod.GET, `/rest/api/3/issue/${key}/transitions`, null, null)
    try {
      let issueMetadataResponse: any = await response.json()
      logger.info("get issue transitions response", issueMetadataResponse)
      return issueMetadataResponse;
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  async transitionIssue(key: string, transitionId: string) {
    let logger = new Logger("JiraClient:transitionIssue", false)
    let body = `{
      "transition": {
          "id" : "${transitionId}"
      }
    }`;
    let response = await this.callJiraApi(HttpMethod.POST, `/rest/api/3/issue/${key}/transitions`, null, body)
    logger.info("update field response status", response.status)
  }

  async createTask(parentKey: string, summary: string, dueDate: string, alarmTime: string){
    let logger = new Logger("JiraClient:createTask", false)
    let body = `{
      "update": {},
      "fields": {
        "summary": "${summary}",
        "parent": {
          "key": "${parentKey}"
        },
        "issuetype": {
          "id": "${TASK_ISSUE_TYPE}"
        },
        "project": {
          "id": "${TASK_PROJECT_ID}"
        },
        "reporter": {
          "id": "557058:2f12732f-687c-4396-9b05-c1fe99179612"
        },
        "duedate": "${dueDate}",
        "assignee": {
          "id": "557058:2f12732f-687c-4396-9b05-c1fe99179612"
        },
        "${ALARM_TIME_CUSTOM_FIELD}": "${alarmTime}"
      }
    }`;
    let response = await this.callJiraApi(HttpMethod.POST, `/rest/api/3/issue`, null, body)
    logger.info("create issue", response.status)
  }

  async getProjects(): Promise<any> {
    let logger = new Logger("JiraClient:getProjects", false)
    let response = await this.callJiraApi(HttpMethod.GET, `/rest/api/3/project/search`, null, null)
    try {
      let projectsResponse: any = await response.json()
      logger.info("get issue transitions response", projectsResponse)
      return projectsResponse;
    } catch (err) {
        console.log(err)
        throw err
    }
  }

  async updateField(key: string, field: string, value: string){
    let logger = new Logger("JiraClient:updateField", false)
    let body = `{
      "fields": {
          "${field}" : ${value}
      }
    }`;
    let response = await this.callJiraApi(HttpMethod.PUT, `/rest/api/3/issue/${key}`, null, body)
    logger.info("update field response status", response.status)
  }
  // async sample(filter: string){
  //   var url = new URL('https://ritwik.atlassian.net/rest/api/3/search')
  //   var params = {
  //     jql: `(Sprint is empty) AND issuetype = TASK AND project = Tasks AND (due >= ${startDate.format('YYYY-MM-DD')} AND due <= ${endDate.format('YYYY-MM-DD')}) AND status != Done AND status != Failed`, 
  //     expand: 'names,renderedFields'
  //   }
  //   url.search = new URLSearchParams(params).toString();
  //   var response: Response = await fetch(url, {
  //   method: 'GET',
  //   headers: {
  //       'Authorization': `Basic ${Buffer.from(
  //       'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
  //       ).toString('base64')}`,
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json'
  //   },
  //   })
  //   try {
  //       let searchResponse: JiraSearchResponse<Task> = new JiraSearchResponse(await response.json(), Task)
  //       return searchResponse.issues
  //   } catch (err) {
  //       console.log(err)
  //       throw err
  //   }
  // }
}