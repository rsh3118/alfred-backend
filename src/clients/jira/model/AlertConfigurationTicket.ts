import { JiraTicket } from "./JiraTicket"
import { Cadence, createCadence } from "./Cadence"
import { Priority } from "./Priority"
import { TimeOfDay } from "../../../model/TimeOfDay"
import { AlertConfiguration } from "../../../model/AlertConfiguration"
import dayjs from "dayjs"
import { Logger } from "../../../util/Logger"
import { ALERTING_METHODS, CALL_AND_TEXT, NOTIFICATION, NOTIFICATION_END_TIME, NOTIFICATION_START_TIME, PREFIX, PUSH_NOTIFICATION, SNOOZE_UNTIL } from "./JiraConstants"
import { convertToDate } from "../../../util/DateUtil"


export class AlertConfigurationTicket extends JiraTicket implements AlertConfiguration {
    alarmStartTimeOfDay: TimeOfDay = new TimeOfDay("00:00")
    alarmEndTimeofDay: TimeOfDay = new TimeOfDay("23:59")
    shouldCallAndText: boolean = false
    shouldNotify: boolean = false
    prefix: string = 'Default'
    snoozeUntil: dayjs.Dayjs | null = null

    constructor(apiResponseIssue: any, keyObject: any){
        super(apiResponseIssue, keyObject)
        let logger = new Logger("AlertConfigurationTicket:constructor", false)
        let debugLogger = new Logger("(debug) AlertConfigurationTicket:constructor", false)
        //logger.info("fields", this.fieldsMap)
        debugLogger.info(NOTIFICATION_START_TIME, this.fieldsMap.get(NOTIFICATION_START_TIME))
        this.alarmStartTimeOfDay = new TimeOfDay(this.fieldsMap.get(NOTIFICATION_START_TIME))
        debugLogger.info(NOTIFICATION_END_TIME, this.fieldsMap.get(NOTIFICATION_END_TIME))
        this.alarmEndTimeofDay = new TimeOfDay(this.fieldsMap.get(NOTIFICATION_END_TIME))
        debugLogger.info(SNOOZE_UNTIL, this.fieldsMap.get(SNOOZE_UNTIL))
        let snoozeUntilString = this.fieldsMap.get(SNOOZE_UNTIL)
        if (snoozeUntilString) {
            snoozeUntilString = snoozeUntilString.substring(0, snoozeUntilString.length - 5)
            debugLogger.info("snooze until string", snoozeUntilString)
            this.snoozeUntil = convertToDate(snoozeUntilString, 'YYYY-MM-DDTYY:HH.mm')
            debugLogger.info("snooze until", this.snoozeUntil)
        }
        debugLogger.info("current time", dayjs())
        debugLogger.info(ALERTING_METHODS, this.fieldsMap.get(ALERTING_METHODS))
        let notificationMethods = []
        if(this.fieldsMap.get(NOTIFICATION)){
            notificationMethods = this.fieldsMap.get(NOTIFICATION).map((obj: { value: any }) => obj.value)
        }
        this.shouldCallAndText = notificationMethods.includes(CALL_AND_TEXT)
        this.shouldNotify = notificationMethods.includes(PUSH_NOTIFICATION)
        this.prefix = this.fieldsMap.get(PREFIX)
        logger.info("alertConfig", {
            "alarmStartTimeOfDay": this.alarmStartTimeOfDay,
            "alarmEndTimeofDay": this.alarmEndTimeofDay,
            "shouldCallAndText": this.shouldCallAndText,
            "shouldNotify": this.shouldNotify,
            "prefix": this.prefix,
            "snoozeUntil": this.snoozeUntil
        })
    }
}