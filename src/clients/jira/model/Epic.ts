import { JiraTicket } from "./JiraTicket"

export class Epic extends JiraTicket {
  multiplier: number
  outstandingGems: number

  constructor(apiResponseIssue: any, keyObject: any){
      super(apiResponseIssue, keyObject)
      this.multiplier = this.fieldsMap.get("Multiplier")
      this.outstandingGems = this.fieldsMap.get("Outstanding Gems")
  }
}