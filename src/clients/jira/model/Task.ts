import dayjs from "dayjs";
import { convertToCST, convertToGMT } from "../../../util/DateUtil";
import { Cadence } from "./Cadence";
import { JiraTicket } from "./JiraTicket";
import { Priority } from "./Priority";

import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 
import { IMPORTANCE_MULTIPLIER, INERTIA_MULTIPLIER, NOVELTY_MULTIPLIER, PRIDE_MULTIPLIER, SPEED_MULTIPLIER } from "./JiraConstants";
import { Logger } from "../../../util/Logger";

dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")

export class Task extends JiraTicket {
    alarmStartTimeInMinutes: number
    alarmEndTimeInMinutes: number
    priority: Priority
    dueDate: dayjs.Dayjs
    storyPoints: number
    noveltyMultiplier: number | null = null
    inertiaMultiplier: number | null = null
    importanceMultiplier: number | null = null
    prideMultiplier: number | null = null
    speedMultiplier: number | null = null
    gemsEarned: number | null = null

    constructor(apiResponseIssue: any, keyObject: any){
        super(apiResponseIssue, keyObject)
        let logger = new Logger("Task:constructor", false)
        this.alarmStartTimeInMinutes = this.fieldsMap.get("Daily Start Time")
        this.alarmEndTimeInMinutes = this.fieldsMap.get("Daily End Time")
        this.storyPoints = this.fieldsMap.get("Story point estimate") || 0
        if(this.fieldsMap.get("Priority")){
            this.priority = this.fieldsMap.get("Priority").name as Priority
        }else{
            this.priority = Priority.LOWEST
        }
        if(apiResponseIssue.fields.duedate){
            this.dueDate = dayjs.tz(apiResponseIssue.fields.duedate)
            //this.dueDate = convertToGMT(this.dueDate)
        }else{
            this.dueDate = dayjs(new Date(10000))
        }
        this.ifExistsDo(
            SPEED_MULTIPLIER, 
            speedMultiplierObj => {
                logger.info("speed multiplier", this.parseMultiplier(speedMultiplierObj.value))
                this.speedMultiplier = this.parseMultiplier(speedMultiplierObj.value)
            }
        )
        this.ifExistsDo(
            INERTIA_MULTIPLIER, 
            inertiaMultiplierObj => this.inertiaMultiplier = this.parseMultiplier(inertiaMultiplierObj.value)
        )
        this.ifExistsDo(
            NOVELTY_MULTIPLIER, 
            noveltyMultiplierObj => this.noveltyMultiplier = this.parseMultiplier(noveltyMultiplierObj.value)
        )
        this.ifExistsDo(
            IMPORTANCE_MULTIPLIER, 
            importanceMultiplierObj => this.importanceMultiplier = this.parseMultiplier(importanceMultiplierObj.value)
        )
        this.ifExistsDo(
            PRIDE_MULTIPLIER, 
            prideMultiplierObj => this.prideMultiplier = this.parseMultiplier(prideMultiplierObj.value)
        )
    }

    private ifExistsDo(field: string, callback:(fieldValue: any) => void): void {
        if(this.fieldsMap.get(field)){
            callback(this.fieldsMap.get(field))
        }
    }

    private parseMultiplier(multiplerValue: string): number {
        let logger = new Logger("Task:parseMultiplier", false)
        return +multiplerValue.split(" ")[0].replace("x", "");
    }
}