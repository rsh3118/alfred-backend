import { Cadence, createCadence } from "./Cadence"
import { JiraTicket } from "./JiraTicket"
import { Priority } from "./Priority"

export class HabitEpic extends JiraTicket {
    alarmStartTimeInMinutes: number
    alarmEndTimeInMinutes: number
    cadence: Cadence
    priority: Priority

    constructor(apiResponseIssue: any, keyObject: any){
        super(apiResponseIssue, keyObject)
        this.alarmStartTimeInMinutes = this.fieldsMap.get("Daily Start Time")
        this.alarmEndTimeInMinutes = this.fieldsMap.get("Daily End Time")
        this.cadence = createCadence(this.fieldsMap.get("Cadence").value)
        this.priority = this.fieldsMap.get("Priority").name as Priority
    }
}