function getCapturedGroup(arr: RegExpMatchArray | null): string{
    if(arr === null){
        throw new Error("regex not matched")
    }
    let group = null
    arr.forEach(function a(value: string, index: number, array: string[]){
        if(index === 1){
            group = value
        }
    })
    if(group === null){
        throw new Error("couldn't find capture group")
    }
    return group
}

export function createCadence(str: string){
    let cadenceValue = str.toLowerCase()
    if(cadenceValue.match(DAILY_CADENCE_REGEX_MATCHER)){
        return new DailyCadence();
    }
    if(cadenceValue.match(WEEKLY_CADENCE_REGEX_MATCHER)){
        return new WeeklyCadence();
    }
    if(cadenceValue.match(MONTHLY_CADENCE_REGEX_MATCHER)){
        return new MonthlyCadence();
    }
    if(cadenceValue.match(EVERYXDAY_CADENCE_REGEX_MATCHER)){
        let group = getCapturedGroup(cadenceValue.match(EVERYXDAY_CADENCE_REGEX_MATCHER))
        return new EveryXDaysCadence(Number(group));
    }
    if(cadenceValue.match(DAY_OF_THE_WEEK_REGEX_MATCHER)){
        let group = getCapturedGroup(cadenceValue.match(DAY_OF_THE_WEEK_REGEX_MATCHER))
        return new DayOfWeekCadence(group as DayOfWeek);
    }
    if(cadenceValue.match(DAY_OF_THE_MONTH_CADENCE_REGEX_MATCHER)){ 
        let group = getCapturedGroup(cadenceValue.match(DAY_OF_THE_MONTH_CADENCE_REGEX_MATCHER))
        return new DayOfTheMonthCadence(group as DayOfMonth);
    }
    if(cadenceValue.match(LAST_DAY_OF_THE_MONTH_CADENCE_REGEX_MATCH)){
        return new LastDayOfTheMonthCadence();
    }
    if(cadenceValue.match(EVERY_WEEKDAY_CADENCE_REGEX_MATCHER)){
        return new WeekdayCadence();
    }
    if(cadenceValue.match(EVERY_WEEKEND_DAY_CADENCE_REGEX_MATCHER)){
        return new WeekendCadence();
    }
    if(cadenceValue.match(EVERYDAY_EXCEPT_FRIDAY_CADENCE)){
        return new EverydayExceptFridayCadence();
    }
    throw new Error("didn't match any cadence")
}

export abstract class Cadence {

}
let DAILY_CADENCE_REGEX_MATCHER = /daily/
export class DailyCadence extends Cadence {
    public toString = () : string => {
        return `Daily Cadence`;
    }
}

let WEEKLY_CADENCE_REGEX_MATCHER = /weekly/
export class WeeklyCadence extends Cadence {
    public toString = () : string => {
        return `Weekly Cadence`;
    }
}

let MONTHLY_CADENCE_REGEX_MATCHER = /monthly/
export class MonthlyCadence extends Cadence {
    public toString = () : string => {
        return `Monthly Cadence`;
    }
}

let EVERYXDAY_CADENCE_REGEX_MATCHER = /every\s([\d]+)\sdays/
export class EveryXDaysCadence extends Cadence {
    x: number
    
    constructor(x: number){
        super()
        this.x = x
    }

    public toString = () : string => {
        return `Every ${this.x} Days Cadence`;
    }
}

let DAY_OF_THE_WEEK_REGEX_MATCHER = /every\s(monday|tuesday|wednesday|thursday|friday|saturday|sunday)/
export enum DayOfWeek {
    MONDAY = "moday",
    TUESDAY = "tuesday",
    WEDNESDAY = "wednesday",
    THURSDAY = "thursday",
    FRIDAY = "friday",
    SATURDAY = "saturday",
    SUNDAY = "sunday"
}
export class DayOfWeekCadence extends Cadence{
    dayOfWeek: DayOfWeek

    constructor(dayOfWeek: DayOfWeek){
        super()
        this.dayOfWeek = dayOfWeek
    }

    public getDayOfWeekNumber(){
        if(this.dayOfWeek == DayOfWeek.MONDAY){
            return 0
        }
        if(this.dayOfWeek == DayOfWeek.TUESDAY){
            return 1
        }
        if(this.dayOfWeek == DayOfWeek.WEDNESDAY){
            return 2
        }
        if(this.dayOfWeek == DayOfWeek.THURSDAY){
            return 3
        }
        if(this.dayOfWeek == DayOfWeek.FRIDAY){
            return 4
        }
        if(this.dayOfWeek == DayOfWeek.SATURDAY){
            return 5
        }
        if(this.dayOfWeek == DayOfWeek.SUNDAY){
            return 6
        }
        throw new Error("Could not map day of week")
    }

    public toString = () : string => {
        return `Every ${this.dayOfWeek} Cadence`;
    }
}

let DAY_OF_THE_MONTH_CADENCE_REGEX_MATCHER = /([\d]+)[\w]*\sof\sthe\smonth/
export enum DayOfMonth {
    DAY_OF_MONTH_1 = '1',
    DAY_OF_MONTH_2 = '2',
    DAY_OF_MONTH_3 = '3',
    DAY_OF_MONTH_4 = '4',
    DAY_OF_MONTH_5 = '5',
    DAY_OF_MONTH_6 = '6',
    DAY_OF_MONTH_7 = '7',
    DAY_OF_MONTH_8 = '8',
    DAY_OF_MONTH_9 = '9',
    DAY_OF_MONTH_10 = '10',
    DAY_OF_MONTH_11 = '11',
    DAY_OF_MONTH_12 = '12',
    DAY_OF_MONTH_13 = '13',
    DAY_OF_MONTH_14 = '14',
    DAY_OF_MONTH_15 = '15',
    DAY_OF_MONTH_16 = '16',
    DAY_OF_MONTH_17 = '17',
    DAY_OF_MONTH_18 = '18',
    DAY_OF_MONTH_19 = '19',
    DAY_OF_MONTH_20 = '20',
    DAY_OF_MONTH_21 = '21',
    DAY_OF_MONTH_22 = '22',
    DAY_OF_MONTH_23 = '23',
    DAY_OF_MONTH_24 = '24',
    DAY_OF_MONTH_25 = '25',
    DAY_OF_MONTH_26 = '26',
    DAY_OF_MONTH_27 = '27',
    DAY_OF_MONTH_28 = '28',
    DAY_OF_MONTH_29 = '29',
    DAY_OF_MONTH_30 = '30',
    DAY_OF_MONTH_31 = '31'
}
export class DayOfTheMonthCadence extends Cadence {
    dayOfMonth: DayOfMonth

    constructor(dayOfMonth: DayOfMonth){
        super()
        this.dayOfMonth = dayOfMonth
    }

    public toString = () : string => {
        return `Every ${this.dayOfMonth} of the Month Cadence`;
    }
}

let LAST_DAY_OF_THE_MONTH_CADENCE_REGEX_MATCH = /last\sday\sof\sthe\smonth/
export class LastDayOfTheMonthCadence extends Cadence {
    public toString = () : string => {
        return `Last Day of Every Month Cadence`;
    }
}

let EVERY_WEEKDAY_CADENCE_REGEX_MATCHER = /every\sday\sof\sthe\sworking\sweek/
export class WeekdayCadence extends Cadence {
    public toString = () : string => {
        return `Every Weekday Cadence`;
    }
}

let EVERY_WEEKEND_DAY_CADENCE_REGEX_MATCHER = /every\sday\sof\sthe\sweekend/
export class WeekendCadence extends Cadence {
    public toString = () : string => {
        return `Every Weekend Cadence`;
    }
}

let EVERYDAY_EXCEPT_FRIDAY_CADENCE = /every\sday\sexcept\sfriday/
export class EverydayExceptFridayCadence extends Cadence {
    public toString = () : string => {
        return `Every Day Except Friday Cadence`;
    }
}

// export enum Cadence {
//     // common cadences
//     DAILY,
//     WEEKLY,
//     MONTHLY,
//     // every x days
//     EVERY_2_DAYS,
//     EVERY_3_DAYS,
//     EVERY_4_DAYS,
//     EVERY_5_DAYS,
//     EVERY_6_DAYS,
//     // only on specific days of the week
//     MONDAY,
//     TUESDAY,
//     WEDNESDAY,
//     THURSDAY,
//     FRIDAY,
//     SATURDAY,
//     SUNDAY,
//     // only on specific days of the month
//     MONTH_1,
//     MONTH_2,
//     MONTH_3,
//     MONTH_4,
//     MONTH_5,
//     MONTH_6,
//     MONTH_7,
//     MONTH_8,
//     MONTH_9,
//     MONTH_10,
//     MONTH_11,
//     MONTH_12,
//     MONTH_13,
//     MONTH_14,
//     MONTH_15,
//     MONTH_16,
//     MONTH_17,
//     MONTH_18,
//     MONTH_19,
//     MONTH_20,
//     MONTH_21,
//     MONTH_22,
//     MONTH_23,
//     MONTH_24,
//     MONTH_25,
//     MONTH_26,
//     MONTH_27,
//     MONTH_28,
//     MONTH_29,
//     MONTH_30,
//     MONTH_31
// }