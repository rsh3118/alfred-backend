export enum Status {
    TODO = "To Do",
    DONE = "Done",
    FAILED = "Failed"
}