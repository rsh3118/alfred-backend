import { Status } from "./Status"
import { Logger } from "../../../util/Logger"
import { SPEED_MULTIPLIER } from "./JiraConstants"

export class JiraTicketParentSummary {
    key: string
    summary: string

    constructor(key: string, summary: string){
        this.key = key
        this.summary = summary
    }
}

export abstract class JiraTicket {
    id: string
    key: string
    summary: string
    fieldsMap: Map<string, any>
    status: Status
    parent: JiraTicketParentSummary | null = null

    constructor(apiResponse: any, keyObject: any){
        let logger = new Logger("JiraTicket: constructor", false)
        this.id = apiResponse.id
        this.key = apiResponse.key
        this.summary = apiResponse.fields.summary
        this.status = apiResponse.fields.status.name as Status
        this.fieldsMap = new Map<string, string>()
        let sampleMap = new Map<string, string>()
        //logger.info("fields", apiResponse.fields)
        logger.info("keyObject", keyObject)
        let fieldIds = Object.getOwnPropertyNames(apiResponse.fields)
        for(let i=0; i < fieldIds.length ; i++){
            let fieldId = fieldIds[i]
            logger.info(fieldId)
            let fieldName = keyObject[fieldId]
            logger.info(fieldName)
            logger.info(apiResponse.fields[fieldId])
            if(fieldName){
                this.fieldsMap.set(fieldName , apiResponse.fields[fieldId])
                sampleMap.set("5", "5")
                logger.info("fielderino", this.fieldsMap.get(fieldName))
                logger.info("sampleMap", sampleMap)
            }
        }
        //logger.info("fieldMap", this.fieldsMap)
        if(this.fieldsMap.get("Parent")){
            this.parent = new JiraTicketParentSummary(this.fieldsMap.get("Parent")["key"], this.fieldsMap.get("Parent")["fields"]["summary"])
        }
        logger.info("parent", this.parent)
    }
}

