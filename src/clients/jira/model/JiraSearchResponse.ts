import { start } from "repl"
import { HabitEpic } from "./HabitEpic";
import { JiraTicket } from "./JiraTicket"


type Constructor<T> = new (...args: any[]) => T;


export class JiraSearchResponse<T> {
    startAt: number
    maxResults: number
    total: number
    issues: Array<T>
    names: Map<string, string>

    constructor(apiResponse: any, constructor: Constructor<T>){
        this.startAt = apiResponse.startAt
        this.maxResults = apiResponse.maxResults
        this.total = apiResponse.total
        this.issues = apiResponse.issues
        this.names = apiResponse.names as Map<string, string>
        if(!Array.isArray(apiResponse.issues)){
            throw new Error()
        }
        this.issues = []
        for(let i=0; i<apiResponse.issues.length; i++){
            let habitEpic = new constructor(apiResponse.issues[i], this.names)
            this.issues.push(habitEpic as T)
        }
    }
}