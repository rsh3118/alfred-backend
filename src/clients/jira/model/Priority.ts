export enum Priority {
    HIGHEST,
    HIGH,
    MEDIUM,
    LOW,
    LOWEST
}