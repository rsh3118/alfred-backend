// Sprint Constant
export const TODAY_SPRINT_ID = '25'
export const TOMORROW_SPRINT_ID = '26'
export const NEXT_WEEK_SPRINT_ID = '27'
export const COMPLETED_ID = '29'

// Sprint Enums
export enum Sprint {
  TODAY = "TODAY",
  TOMORROW = "TOMORROW",
  NEXT_WEEK = "NEXT_WEEK",
  COMPLETED = "COMPLETED"
}

// Alert Config Constants
export const LOW_PRIORITY_ALERT_CONFIG_KEY = 'CONFIG-3'
export const MEDIUM_PRIORITY_ALERT_CONFIG_KEY = 'CONFIG-5'
export const HIGH_PRIORITY_ALERT_CONFIG_KEY = 'CONFIG-4'

// field names
export const SNOOZE_UNTIL = "Snooze Until"
export const ALERTING_METHODS = "Alerting Methods"
export const NOTIFICATION_START_TIME = "Notification Start Time"
export const NOTIFICATION_END_TIME = "Notification End Time"
export const NOTIFICATION = "Notification"
export const PREFIX = "Prefix";
export const SPEED_MULTIPLIER = "Speed Multiplier"
export const PRIDE_MULTIPLIER = "Pride Multiplier"
export const IMPORTANCE_MULTIPLIER = "Importance Multiplier"
export const INERTIA_MULTIPLIER = "Inertia Multiplier"
export const NOVELTY_MULTIPLIER = "Novelty Multiplier"
// Notification Methods
export const PUSH_NOTIFICATION = "Push Notification"
export const CALL_AND_TEXT = "Call & Text"
// Custom Fields
export const GEMS_EARNED_FIELD = "customfield_10088";
// Status
export const TO_DO_STATUS = "To Do";
// Transition
export const FAILED_TRANSITION = "31";
// Project Ids
export const TASK_PROJECT_ID = "10000";
// Issue Type Ids
export const TASK_ISSUE_TYPE = "10001";
// Custom Fields
export const ALARM_TIME_CUSTOM_FIELD = "customfield_10091"


