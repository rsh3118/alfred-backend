import { Logger } from "../util/Logger";

var opsgeniePhoneInstance: any = require('opsgenie-sdk');
var opsgenieNotificationInstance: any = require('opsgenie-sdk');



export enum OpsgenieAction {
    CALL_AND_TEXT = "CALL AND TEXT",
    PUSH_NOTIFICATION = "PUSH_NOTIFICATION"
}

export type OpsgenieAlert = {
    message: string
}

export class OpsgenieClient {

    constructor(){}

    createAlert(action: OpsgenieAction, message: string){
        let logger = new Logger("OpsgenieClient:createAlert", false)
        let alert: OpsgenieAlert = {message}
        let opsgenieInstance = opsgenieNotificationInstance
        switch(action){
            case OpsgenieAction.CALL_AND_TEXT:
                opsgeniePhoneInstance.configure({
                    'api_key': '7fb556f4-1b33-48c4-8191-ed2e024a40c7'
                })
                break;
            case OpsgenieAction.PUSH_NOTIFICATION:
                opsgenieNotificationInstance.configure({
                    'api_key': '1f3aa085-f8f8-4704-91ff-98cca4ffb879'
                });
                break;
            default:
                break;
        }

        opsgenieInstance.alertV2.create(alert, function (error: any, alert: any) {
            if (error) {
                logger.error(error);
            } else {
                logger.info(alert);
            }
        });
    }

}