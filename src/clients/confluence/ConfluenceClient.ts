import dayjs from "dayjs";
import { HTMLElement, Node } from "node-html-parser";
import { ConfluencePage } from "./model/ConfluencePage";
import { ConfluenceSearchResponse, SearchResult } from "./model/ConfluenceSearchResponse";
import { JiraIssuesMacro } from "./model/JiraIssuesMacro";

import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 

dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")

const fetch = require('node-fetch');

export class ConfluenceClient {
    constructor(){
    }

    elementContainsUnorderedListElement(currentHTMLElement: HTMLElement): boolean{
        let nodeArray: Array<Node> = currentHTMLElement.childNodes
        for(let i = 0; i < nodeArray.length; i++ ){
            let childNode: Node = nodeArray[i]
            try {
                let childhtmlElement: HTMLElement = childNode as HTMLElement
                this.checkForHabitHeaders(childhtmlElement)
                if(childhtmlElement.tagName == 'UL'){
                    return true
                }
            } catch (err) {

            }
        }
        return false
    }

    checkForHabitList(currentHTMLElement: HTMLElement){
        let nodeArray: Array<Node> = currentHTMLElement.childNodes
        for(let i = 0; i < nodeArray.length; i++ ){
            let childNode: Node = nodeArray[i]
            try {
                let childhtmlElement: HTMLElement = childNode as HTMLElement
                if(childhtmlElement.tagName == 'H2' && childhtmlElement.text == 'Habits'){
                    if(i == nodeArray.length - 1){
                    }
                    let maybeUnorderedListElement = nodeArray[i + 1] as HTMLElement
                    if(maybeUnorderedListElement.tagName == 'UL'){
                    } else {
                    }
                }
            } catch (err) {

            }
        }
        return false
    }

    checkForHabitHeaders(currentHTMLElement: HTMLElement): HTMLElement | undefined  {
        let nodeArray: Array<Node> = currentHTMLElement.childNodes
        for(let i = 0; i < nodeArray.length; i++ ){
            let childNode: Node = nodeArray[i]
            try {
                let childhtmlElement: HTMLElement = childNode as HTMLElement
                let maybeHabitHeaderHTMLElement = this.checkForHabitHeadersHelper(currentHTMLElement, childhtmlElement)
                if (maybeHabitHeaderHTMLElement != undefined){
                    return maybeHabitHeaderHTMLElement
                }
            } catch (err) {

            }
        }
        return undefined
    }

    checkForHabitHeadersHelper(parentHTMLElement: HTMLElement, currentHTMLElement: HTMLElement): HTMLElement | undefined {
        if(currentHTMLElement.tagName == undefined && parentHTMLElement.tagName == 'H2' && currentHTMLElement.text == "Habits") {
            return parentHTMLElement
        }
        let nodeArray: Array<Node> = currentHTMLElement.childNodes
        for(let i = 0; i < nodeArray.length; i++ ){
            let childNode: Node = nodeArray[i]
            try {
                let childhtmlElement: HTMLElement = childNode as HTMLElement
                let maybeHabitHeaderHTMLElement = this.checkForHabitHeadersHelper(currentHTMLElement, childhtmlElement)
                if (maybeHabitHeaderHTMLElement != undefined){
                    return maybeHabitHeaderHTMLElement
                }
            } catch (err) {

            }
        }
        return undefined
    }

    async getAllContent(){
        var url = new URL('https://ritwik.atlassian.net/wiki/rest/api/content')
        var response: Response = await fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Basic ${Buffer.from(
            'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
            ).toString('base64')}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        })
        try {
            let text: string = await response.text()
        } catch (err) {
        }
    }

    async getReflectionPagesIds(): Promise<Array<string>>{
        var url = new URL('https://ritwik.atlassian.net/wiki/rest/api/search')
        var params = {
            cql: 'title ~ "Reflection ??/??/??"'
        }
        url.search = new URLSearchParams(params).toString();
        var response: Response = await fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Basic ${Buffer.from(
            'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
            ).toString('base64')}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        })
        try {
            let searchResponse: ConfluenceSearchResponse
            searchResponse = new ConfluenceSearchResponse(await response.json())
            const regex = /Reflection \d{2}\/\d{2}\/(\d{4}|\d{2})/g;
            return searchResponse.results
                .filter(searchResult => searchResult.content.title.match(regex) != undefined)
                .map(searchResult => searchResult.content.id)
        } catch (err) {
            console.log(err)
        }
        return []
	}
	
	async getDailyReflectionPageByDate(date: dayjs.Dayjs): Promise<Array<string>>{
        var url = new URL('https://ritwik.atlassian.net/wiki/rest/api/search')
        var params = {
            cql: `title ~ "${date.format("MM/DD/YY")} Reflection"`
        }
        url.search = new URLSearchParams(params).toString();
        var response: Response = await fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Basic ${Buffer.from(
            'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
            ).toString('base64')}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        })
        try {
            let searchResponse: ConfluenceSearchResponse
            searchResponse = new ConfluenceSearchResponse(await response.json())
            const regex = /Reflection \d{2}\/\d{2}\/(\d{4}|\d{2})/g;
            return searchResponse.results
                .map(searchResult => searchResult.content.id)
        } catch (err) {
            console.log(err)
        }
        return []
    }

    async getConfluencePage(id: string): Promise<ConfluencePage>{
        var url = new URL('https://ritwik.atlassian.net/wiki/rest/api/content/' + id)
        var params = {
            expand: 'body.storage'
        }
        url.search = new URLSearchParams(params).toString();
        var response: Response = await fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Basic ${Buffer.from(
            'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
            ).toString('base64')}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        })
        try {
            let res = await response.json()
            let confluencePage: ConfluencePage = new ConfluencePage(res)
            return confluencePage
        } catch (err) {
            throw err;
        }
    }

    async createReflectionPage(title: string, content: string){
        let re = /<br>/gi;
        var url = new URL('https://ritwik.atlassian.net/wiki/rest/api/content')
        let bodyData = {
            title,
            type: "page",
            space: {
                key: "REFLECTION"
            },
            body: {
                storage: {
                    value: content,
                    representation: "storage"
                }
            },
            metadata: {
                properties: {
                    editor: {
                        key: "editor",
                        value: "v2"
                    }
                }
            }
        }
        let bodyDataString: string = JSON.stringify(bodyData)
        var response: Response = await fetch(url, {
        method: 'POST',
        headers: {
            'Authorization': `Basic ${Buffer.from(
            'rsh3118@gmail.com:l4IcH8348aOwLNg05CKi0F5D'
            ).toString('base64')}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: bodyDataString
        })
        try {
            let text: string = await response.text()
        } catch (err) {
            throw err;
        }

        function b(){
			let successes = new JiraIssuesMacro(
				`Successes`,
				`project = REFLECTION AND created >= "2021/02/08 00:00" AND created <= "2021/02/08 23:59" AND status = "SUCCESS"`,
				[`key`, `summary`, `status`],
				50
			)

			let failures = new JiraIssuesMacro(
				`Failures`,
				`project = REFLECTION AND created >= "2021/02/08 00:00" AND created <= "2021/02/08 23:59" AND status = "FAILURE"`,
				[`key`, `summary`, `status`],
				50
			)

			let observations = new JiraIssuesMacro(
				`Observations`,
				`project =REFLECTION AND created >= "2021/02/08 00:00" AND created <= "2021/02/08 23:59" AND status = "OBSERVATION"`,
				[`key`, `summary`, `status`],
				50
			)

			let tasksCompleted = new JiraIssuesMacro(
				`Tasks Completed`,
				`project != Acts AND resolved >= "2021/02/08 00:00" AND resolved <= "2021/02/08 23:59"`,
				[`key`, `summary`, `status`],
				50
			)

			let habits = new JiraIssuesMacro(
				`Habits`,
				`project = HABIT AND text ~ "02/07/21"`,
				[`key`, `summary`, `status`],
				50
			)

			let activities = new JiraIssuesMacro(
				`Activities`,
				`project = REFLECTION AND created >= "2021/02/08 00:00" AND created <= "2021/02/08 23:59" AND status = "FAILURE"`,
				[`key`, `summary`, `status`],
				50
			)

			let a = `
			${successes.renderXML()}
			${failures.renderXML()}
			${observations.renderXML()}
			${tasksCompleted.renderXML()}
			${habits.renderXML()}
			${activities.renderXML()}
			`
			a = a.split("\n").map(line => line.trim()).join("\n")

			return a

        }

        function a(){
            return `
<h2>Successes</h2>
<p />
<ac:structured-macro ac:name="jira" ac:schema-version="1" data-layout="default" ac:macro-id="48a4d1b0-02ec-4231-82ff-83ac7c7309a6">
	<ac:parameter ac:name="server">System JIRA</ac:parameter>
	<ac:parameter ac:name="columns">key,summary,status</ac:parameter>
	<ac:parameter ac:name="maximumIssues">20</ac:parameter>
	<ac:parameter ac:name="jqlQuery">project =REFLECTION AND created >= &quot;2021/02/08 00:00&quot; AND created <= &quot;2021/02/08 23:59&quot; AND status = &quot;SUCCESS&quot;                 
	</ac:parameter>
	<ac:parameter ac:name="serverId">dfdaf529-3f84-3696-83b8-07f5d2c276bd</ac:parameter>
</ac:structured-macro>
<p />
<h2>Failures</h2>
<ac:structured-macro ac:name="jira" ac:schema-version="1" data-layout="default" ac:macro-id="9fe5a9bd-a7e4-4e9c-a671-464c0a40fb93">
	<ac:parameter ac:name="server">System JIRA</ac:parameter>
	<ac:parameter ac:name="columns">key,summary,status</ac:parameter>
	<ac:parameter ac:name="maximumIssues">20</ac:parameter>
	<ac:parameter ac:name="jqlQuery">project =REFLECTION AND created >= &quot;2021/02/08 00:00&quot; AND created <= &quot;2021/02/08 23:59&quot; AND status = &quot;FAILURE&quot;                 
	</ac:parameter>
	<ac:parameter ac:name="serverId">dfdaf529-3f84-3696-83b8-07f5d2c276bd</ac:parameter>
</ac:structured-macro>
<h2>Observations
	<br />
</h2>
<ac:structured-macro ac:name="jira" ac:schema-version="1" data-layout="default" ac:macro-id="674acc15-7a28-44e6-b6b1-89880f617d95">
	<ac:parameter ac:name="server">System JIRA</ac:parameter>
	<ac:parameter ac:name="columns">key,summary,status</ac:parameter>
	<ac:parameter ac:name="maximumIssues">20</ac:parameter>
	<ac:parameter ac:name="jqlQuery">project =REFLECTION AND created >= &quot;2021/02/08 00:00&quot; AND created <= &quot;2021/02/08 23:59&quot; AND status = &quot;OBSERVATION&quot;                 
	</ac:parameter>
	<ac:parameter ac:name="serverId">dfdaf529-3f84-3696-83b8-07f5d2c276bd</ac:parameter>
</ac:structured-macro>
<h2>
	<br />Tasks Completed Today
</h2>
<p />
<ac:structured-macro ac:name="jira" ac:schema-version="1" data-layout="default" ac:macro-id="691cd150-b61f-4748-a228-65a135704a32">
	<ac:parameter ac:name="server">System JIRA</ac:parameter>
	<ac:parameter ac:name="columns">key,summary,due</ac:parameter>
	<ac:parameter ac:name="maximumIssues">20</ac:parameter>
	<ac:parameter ac:name="jqlQuery">project != Acts AND resolved >= &quot;2021/02/08 00:00&quot; AND resolved <= &quot;2021/02/08 23:59&quot;                  
	</ac:parameter>
	<ac:parameter ac:name="serverI">dfdaf529-3f84-3696-83b8-07f5d2c276bd</ac:parameter>
</ac:structured-macro>
<h2>Habits</h2>
<ac:structured-macro ac:name="jira" ac:schema-version="1" data-layout="default" ac:macro-id="62963a8b-b64e-457b-9a95-37bbe899c4a9">
	<ac:parameter ac:name="server">System JIRA</ac:parameter>
	<ac:parameter ac:name="columns">key,summary,status</ac:parameter>
	<ac:parameter ac:name="maximumIssues">20</ac:parameter>
	<ac:parameter ac:name="jqlQuery">project = HABIT AND text ~ &quot;02/07/21&quot;                           </ac:parameter>
	<ac:parameter ac:name="serverId">dfdaf529-3f84-3696-83b8-07f5d2c276bd</ac:parameter>
</ac:structured-macro>
<ac:structured-macro ac:name="info" ac:schema-version="1" ac:macro-id="7b23bbad-04c6-470a-8050-a6f3ebee9dc6">
	<ac:rich-text-body>
		<p>
			<strong>Efficiencies</strong>
		</p>
		<p>
			<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="a8bd2725-da4a-44f3-b7dd-1f7bb0712c50">
				<ac:parameter ac:name="title">BAD</ac:parameter>
				<ac:parameter ac:name="colour">Red</ac:parameter>
			</ac:structured-macro>
			<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="5926943f-fc17-4156-9508-7be0e1df40c0">
				<ac:parameter ac:name="title">SUBPAR</ac:parameter>
				<ac:parameter ac:name="colour">Yellow</ac:parameter>
			</ac:structured-macro>
			<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="7cc4a08b-ed65-4841-b38b-d2c5a7318076">
				<ac:parameter ac:name="title">OK</ac:parameter>
			</ac:structured-macro>
			<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="5c17740f-aa46-47f2-87eb-0340a39ff8c7">
				<ac:parameter ac:name="title">GOOD</ac:parameter>
				<ac:parameter ac:name="colour">Green</ac:parameter>
			</ac:structured-macro>
			<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="1f39f477-e6b5-4c4e-ad26-6a49bd010047">
				<ac:parameter ac:name="title">EXCELLENT</ac:parameter>
				<ac:parameter ac:name="colour">Purple</ac:parameter>
			</ac:structured-macro>
		</p>
	</ac:rich-text-body>
</ac:structured-macro>
<p />
<table data-layout="default">
	<colgroup>
		<col style="width: 211.0px;" />
		<col style="width: 409.0px;" />
		<col style="width: 140.0px;" />
	</colgroup>
	<tbody>
		<tr>
			<th>
				<p>
					<strong>Time</strong>
				</p>
			</th>
			<th>
				<p>
					<strong>Activity</strong>
				</p>
			</th>
			<th>
				<p>
					<strong>Efficiency</strong>
				</p>
			</th>
		</tr>
		<tr>
			<td>
				<p>12 - 2</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-20" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-20</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="78c72d9e-edb0-44c3-a5ea-493328f98821">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>2 - 2:30 </p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-25" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-25</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="5ea5909d-1553-49ed-8047-7289a0c63bf9">
						<ac:parameter ac:name="title">GOOD</ac:parameter>
						<ac:parameter ac:name="colour">Green</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>2:30 - 3</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-50" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-50</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="f504d005-67ca-439c-b8d1-95198ea31817">
						<ac:parameter ac:name="title">GOOD</ac:parameter>
						<ac:parameter ac:name="colour">Green</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>3 - 9:15</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-1" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-1</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="bfda7d48-2042-4307-a02a-3a11098662c0">
						<ac:parameter ac:name="title">SUBPAR</ac:parameter>
						<ac:parameter ac:name="colour">Yellow</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>9:15 - 9:45</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-13" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-13</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="bfa83985-9302-4296-847f-431ae4ae649f">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>9:45 - 10</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-16" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-16</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="239e2e75-ebe3-4191-be3e-10cd7587cda8">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>10 - 11</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-19" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-19</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="c35ae609-7cb1-40d5-b8fe-7c48c531fa8f">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>11 - 11:30</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-27" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-27</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="7be8e01d-2f24-4cf6-80fd-addddd711028">
						<ac:parameter ac:name="title">SUBPAR</ac:parameter>
						<ac:parameter ac:name="colour">Yellow</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>11:30 - 12</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/WORK-107" data-card-appearance="inline">https://ritwik.atlassian.net/browse/WORK-107</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="b1f184a9-f2e3-4ae6-af52-daa72ca6bdf2">
						<ac:parameter ac:name="title">SUBPAR</ac:parameter>
						<ac:parameter ac:name="colour">Yellow</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>12 - 12:30</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-19" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-19</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="44d34fab-438d-46c8-ad19-fd1ea08c7082">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>12:30 - 1</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-23" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-23</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="ee24d1b5-a022-437b-a759-9a33176cfafa">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>1 - 2</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-51" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-51</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="f4272929-7412-446b-b268-2b899bbac2c8">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>2 - 2:30</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-4" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-4</a>
				</p>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-26" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-26</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="bc293e21-4aff-41f8-8e1a-e8b81acb54e1">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>2:30 - 3</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/WORK-107" data-card-appearance="inline">https://ritwik.atlassian.net/browse/WORK-107</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="4bb7e0e9-0585-4c19-a2d1-98b0814a7126">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>3 - 3:45</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-19" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-19</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="221eaa7e-e691-4e89-8a18-f6bae3b11742">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>3:45 - 4</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-49" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-49</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="8535b7c6-3a9a-4105-92a9-15593c7f0528">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>4 - 4:30</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/WORK-101" data-card-appearance="inline">https://ritwik.atlassian.net/browse/WORK-101</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="2e3eca80-89a5-4d31-bf7c-29d3380c360b">
						<ac:parameter ac:name="title">GOOD</ac:parameter>
						<ac:parameter ac:name="colour">Green</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>4:30 - 5</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-44" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-44</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="1b7df5da-c878-4032-a85e-687073e54ae0">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>5 - 6</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-16" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-16</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="91dd96de-ccc7-4a01-b47d-89ce4bd65114">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>6 - 7:30</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-6" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-6</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="dc435951-f6bc-41f5-99b4-5087f2f5b63d">
						<ac:parameter ac:name="title">GOOD</ac:parameter>
						<ac:parameter ac:name="colour">Green</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>7:30 - 8</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-7" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-7</a>
				</p>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-12" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-12</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="77243bdc-7bb6-4753-b4d7-0867840f6653">
						<ac:parameter ac:name="title">GOOD</ac:parameter>
						<ac:parameter ac:name="colour">Green</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>8 - 9:15</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-44" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-44</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="221e1281-d5de-41fe-aa15-91f1b5b7b519">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>9:15 - 10:15 </p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-43" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-43</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="a0c798ee-ec2a-4382-a2b1-2f52d685be27">
						<ac:parameter ac:name="title">SUBPAR</ac:parameter>
						<ac:parameter ac:name="colour">Yellow</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>10:15 - 12</p>
			</td>
			<td>
				<p>
					<a href="https://ritwik.atlassian.net/browse/ACT-20" data-card-appearance="inline">https://ritwik.atlassian.net/browse/ACT-20</a>
				</p>
			</td>
			<td>
				<p>
					<ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="5f97368a-cdb5-4e00-a844-fe1a341ff096">
						<ac:parameter ac:name="title">OK</ac:parameter>
					</ac:structured-macro>
				</p>
			</td>
		</tr>
	</tbody>
</table>
<p />
            `
        }
    }
}   