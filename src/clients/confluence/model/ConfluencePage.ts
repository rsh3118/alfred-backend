import { HTMLElement, parse } from 'node-html-parser';

export class ConfluencePage {
    id: string
    type: string
    status: string
    title: string
    content: HTMLElement

    constructor(apiResponse: any){
        this.id = apiResponse.id
        this.type = apiResponse.type
        this.status = apiResponse.status
        this.title = apiResponse.title
        var doc = parse('<p>Woke up to hear that an Atlassian wide change on the classification of the data classification of AAID from restricted identifier &rarr; pseudonymous identifier was instigated by me pushing the privacy and security for a more wholistic approach on this (mostly cause they said) we couldn&rsquo;t log it<br /><p/><br />Ganondorf was happy with the change and all the members of the team gave me kudos which was nice.<br /><br />I&rsquo;m also happy because Ganondorf found out about the change from his colleagues and said I was living the Atlassian value of &ldquo;Be the change you seek&rdquo;</p>');
        var doc2 = parse(apiResponse.body.storage.value)
        this.content = doc2
    }
}