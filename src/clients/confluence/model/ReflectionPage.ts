import dayjs from "dayjs";
import { JiraIssuesMacro } from "./JiraIssuesMacro";

import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 

dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")

export abstract class ReflectionPage {
    macrosList: JiraIssuesMacro[] = []

    renderXML(){
        let xml = this.macrosList.map(macro => macro.renderXML()).join("\n")
        return xml
    }
}

export class DailyReflectionPage extends ReflectionPage{
    date: dayjs.Dayjs

    constructor(date: dayjs.Dayjs){
        super()
        this.date = date

        let successes = new JiraIssuesMacro(
            `Successes`,
            `project = REFLECTION AND created >= "${date.format("YYYY/MM/DD")} 00:00" AND created <= "${date.format("YYYY/MM/DD")}  23:59" AND status = "SUCCESS"`,
            [`key`, `summary`, `status`],
            50
        )

        let failures = new JiraIssuesMacro(
            `Failures`,
            `project = REFLECTION AND created >= "${date.format("YYYY/MM/DD")} 00:00" AND created <= "${date.format("YYYY/MM/DD")} 23:59" AND status = "FAILURE"`,
            [`key`, `summary`, `status`],
            50
        )

        let observations = new JiraIssuesMacro(
            `Observations`,
            `project =REFLECTION AND created >= "${date.format("YYYY/MM/DD")} 00:00" AND created <= "${date.format("YYYY/MM/DD")} 23:59" AND status = "OBSERVATION"`,
            [`key`, `summary`, `status`],
            50
        )

        let tasksCompleted = new JiraIssuesMacro(
            `Tasks Completed`,
            `project != Acts AND resolved >= "${date.format("YYYY/MM/DD")} 00:00" AND resolved <= "${date.format("YYYY/MM/DD")} 23:59"`,
            [`key`, `summary`, `status`],
            50
        )

        let habits = new JiraIssuesMacro(
            `Habits`,
            `project = HABIT AND text ~ "${date.format("MM/DD/YY")}"`,
            [`key`, `summary`, `status`],
            50
        )

        let activities = new JiraIssuesMacro(
            `Activities`,
            `project = ACT AND issuetype = Activity AND summary ~ "${date.format("MM/DD/YY")}"`,
            [`key`, `summary`, `Efficiency`],
            50
        )

        this.macrosList = [successes, failures, observations, tasksCompleted, habits, activities]
    }
}