export class ConfluenceSearchResponse {
    results: Array<SearchResult>
    start: number
    limit: number
    size: number
    cqlQuery: string

    constructor(apiResponse: any){
        this.results = createSearchResultArray(apiResponse.results)
        this.start = apiResponse.start
        this.limit = apiResponse.limit
        this.size = apiResponse.size
        this.cqlQuery = apiResponse.cqlQuery
    }
}

export function createSearchResultArray(apiResponse: any): Array<SearchResult>{
    if(!Array.isArray(apiResponse)){
        throw new Error("Search Response results are not an array")
    }
    var resultArray: Array<SearchResult> = []
    apiResponse.forEach((value: any, index: number, array: any[]) => {
        try {
            resultArray.push(new SearchResult(value))
        } catch (err) {

        }
    })
    return resultArray
}

export class SearchResult {
    content: SearchResultContent
    url: string
    entityType: any
    title: string
    lastModified: Date

    constructor(apiResponse: any){
        this.content = new SearchResultContent(apiResponse.content)
        this.entityType = apiResponse.entityType
        this.title = apiResponse.title
        this.url = apiResponse.url
        this.lastModified = new Date(apiResponse.lastModified)
    }
}

export class SearchResultContent {
    id: string
    type: string
    status: string
    title: string

    constructor(apiResponse: any) {
        this.id = apiResponse.id
        this.type = apiResponse.type
        this.status = apiResponse.status
        this.title = apiResponse.title
    }
}