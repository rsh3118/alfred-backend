import express, {Application, Request, Response, NextFunction, response} from 'express';
import { ConfluenceClient } from './clients/confluence/ConfluenceClient';
import { JiraService } from './services/JiraService';
import { createCadence } from './clients/jira/model/Cadence';
import { OpsgenieClient } from './clients/OpsgenieClient';
import { HabitTrackingService } from './services/HabitTrackingService';
import { ReflectionService } from './services/ReflectionService';
import { SprintService } from './services/SprintService';
import { AlertingService, AlertPriority } from './services/AlertingService';
import { getRandomInt } from './util/GeneralUtil';
import { Logger } from './util/Logger';
import { GEMS_EARNED_FIELD, Sprint, TODAY_SPRINT_ID, TO_DO_STATUS } from './clients/jira/model/JiraConstants';
import { JiraClient } from './clients/jira/JiraClient';
import { HttpMethod } from './clients/RestAPIConstants';

var opsgenie: any = require('opsgenie-sdk');

const app: Application = express();

const opsgenieClient = new OpsgenieClient()

const jiraClient = new JiraClient();

const confluenceClient = new ConfluenceClient();

const habitTrackingService = new HabitTrackingService();

const reflectionService = new ReflectionService();

const sprintService = new SprintService();

const alertingService = new AlertingService();

const jiraService = new JiraService();


app.get('/', (request: Request, response: Response, next: NextFunction) => {
    response.send('Hello')
    var requestId = "yourRequestId";

    var create_alert_json = {
        "message": "Hello world my alert :)"
    };
});

app.get('/hello', (request: Request, response: Response, next: NextFunction) => {
    response.send('Hello')
    var requestId = "yourRequestId";

    var create_alert_json = {
        "message": "Hello world my alert :)"
    };

    //opsgenieClient.createAlert(create_alert_json)
    //jiraClient.getIssueCreateMeta()
    //confluenceClient.getAllContent()
    //jiraClient.getHabitsProjectIssues()
    //confluenceClient.getAllContent()
    //confluenceClient.getReflectionPagesIds()
    //habitTrackingService.createDailyHabits()
    //habitTrackingService.expireOldHabitTasks()

});

app.get('/publishReflectionPages', (request: Request, response: Response, next: NextFunction) => {
    
    reflectionService.createDailyReflectionPage()

    response.send('success')
});

app.get('/updateHabits', (request: Request, response: Response, next: NextFunction) => {
    
    habitTrackingService.createDailyHabits()
    habitTrackingService.expireOldHabitTasks()

    response.send('success')
});

app.get('/turnoverSprint', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/turnoverSprint", false)
    logger.info("/turnoverSprint request params", request.query)
    await sprintService.turnoverSprint(
        Sprint[request.query["sprint"] as keyof typeof Sprint]
    )
    response.send('success')
});

app.get('/turnoverBacklog', async (request: Request, response: Response, next: NextFunction) => {
    
    await sprintService.turnoverBacklog()


    response.send('success')
});


app.get('/setDefaultStoryPoints', async (request: Request, response: Response, next: NextFunction) => {
    
    await sprintService.setDefaultStoryPoints()


    response.send('success')
});

app.get('/setDefaultDueDate', async (request: Request, response: Response, next: NextFunction) => {
    
    await sprintService.setDefaultDueDate()


    response.send('success')
});

app.get('/sendAlert', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/sendAlert", false)
    logger.info(request.query["message"] as string)
    logger.info("/sendAlert request params", request.query)
    await alertingService.sendAlert(
        AlertPriority[request.query["priority"] as keyof typeof AlertPriority],
        request.query["message"] as string
    )
    response.send('success')
});

app.get('/test', async (request: Request, response: Response, next: NextFunction) => {
    
    await jiraClient.updateField("TASK-792", GEMS_EARNED_FIELD, `${10}`)

    response.send('success')
});

app.get('/computeGems', async (request: Request, response: Response, next: NextFunction) => {

   let responseBody = await sprintService.calculateGemValues()

    response.send(responseBody)
});

app.get('/reloadAlarmConfigurations', async (request: Request, response: Response, next: NextFunction) => {

    let responseBody = await alertingService.setAlarmConfigurations()
 
     response.send('success')
 });

 app.get('/searchIssues', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/getIssueMetaData", false)
    let filter = request.query["filter"] as string
    var params = {
        jql: filter, 
        expand: 'names,renderedFields',
        startAt: 0
    }
    logger.info("params", params)
    let responseBody = (await (await jiraClient.callJiraApi(HttpMethod.GET, jiraClient.searchEndpoint, params, null)).json())
    logger.info("responseBody", responseBody)

 
     response.send(responseBody)
 });

 app.get('/getIssueMetaData', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/getIssueMetaData", false)
    let key = request.query["key"] as string
    logger.info("key", key)
    let responseBody = await jiraClient.getIssueMetadata(key)
    
 
     response.send(responseBody)
 }); 

 app.get('/getIssueTransitions', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/getIssueTransitions", false)
    let key = request.query["key"] as string
    logger.info("key", key)
    let responseBody = await jiraClient.getIssueTransitions(key)
    
 
     response.send(responseBody)
 }); 

 app.get('/transitionIssueToFail', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/getIssueTransitions", false)
    let key = request.query["key"] as string
    logger.info("key", key)
    let responseBody = await jiraService.transitionIssueToFailed(key)
    
 
     response.send(responseBody)
 }); 

 app.get('/getProjects', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/getProjects", false)
    let responseBody = await jiraClient.getProjects()
     response.send(responseBody)
 });

 app.get('/createIssue', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/createIssue", false)
    let parentKey = request.query["parentKey"] as string
    let summary = request.query["summary"] as string
    let dueDate = request.query["dueDate"] as string
    let alarmTime = request.query["alarmTime"] as string
    logger.info("params", {parentKey, summary, dueDate, alarmTime})
    let responseBody = await jiraClient.createTask(parentKey, summary, dueDate, alarmTime)
    
 
     response.send(responseBody)
 });

 app.get('/createHabit', async (request: Request, response: Response, next: NextFunction) => {
    let logger = new Logger("app.ts/createHabit", false)
    let parentKey = request.query["parentKey"] as string
    let summary = request.query["summary"] as string
    summary = summary.split("_").join(" ")
    logger.info("summary", summary)
    let minutesToComplete = +(request.query["minutesToComplete"] as string)
    logger.info("params", {parentKey, summary, minutesToComplete})
    let responseBody = await habitTrackingService.createHabit(parentKey, summary, minutesToComplete)
     response.send(responseBody)
 });

 app.get('/alertOnPendingTasks', async (request: Request, response: Response, next: NextFunction) => {
    let responseBody = await alertingService.alertOnPendingTasks()
 
     response.send('success')
 });
    

app.listen(process.env.PORT || 5000, () => console.log('Server running'))