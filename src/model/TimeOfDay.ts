import { isRegExp } from "util"

export class TimeOfDay {
  hour: number
  minute: number

  constructor(timeOfDayString: string){
    let timeOfDayArray = timeOfDayString.split(":")
    let timeInHours = timeOfDayArray[0]
    let timeInMinutes = timeOfDayArray[1]
    if(+timeInHours < 0 || +timeInHours > 23){
      throw new Error(`time in hours for ${timeOfDayString} must be between 0 and 23`)
    }
    if(+timeInMinutes < 0 || +timeInMinutes > 60){
      throw new Error(`time in minutes for ${timeOfDayString} must be between 0 and 60`)
    }
    this.hour = +timeInHours
    this.minute = +timeInMinutes
  }

  private getFormattedTimeFieldString(timeFieldValue: number): string {
    return '0'.repeat( Math.max(2 - timeFieldValue.toString().length, 0))
  }


  public toString = () : string => {
    return `${this.getFormattedTimeFieldString(this.hour)}:${this.getFormattedTimeFieldString(this.hour)}`;
  }
}