export class CalculateGemsResponse {
  tasksWithError: Array<TaskWithErrorDetails>
  summaryByEpic: Array<EpicGemSummary>
  summaryByEpicWithTaskDetails: Array<EpicGemSummaryWithTaskDetails>
  
  constructor(
    tasksWithError: Array<TaskWithErrorDetails>,
    summaryByEpic: Array<EpicGemSummary>,
    summaryByEpicWithTaskDetails: Array<EpicGemSummaryWithTaskDetails>
  ){
    this.tasksWithError = tasksWithError
    this.summaryByEpic = summaryByEpic
    this.summaryByEpicWithTaskDetails = summaryByEpicWithTaskDetails
  }
}

export class TaskWithErrorDetails {
  taskKey: string
  taskSummary: string
  parentSummary: string | null
  errors: Array<string>

  constructor(taskKey: string, taskSummary: string, parentSummary: string | null, errors: Array<string>){
    this.taskKey = taskKey
    this.taskSummary = taskSummary
    this.parentSummary = parentSummary
    this.errors = errors
  }
}

export class EpicGemSummary {
  epicName: string
  epicGemTotal: number

  constructor(epicName: string, epicGemTotal: number){
    this.epicName = epicName
    this.epicGemTotal = epicGemTotal
  }
}

export class EpicGemSummaryWithTaskDetails {
  epicName: string
  epicGemTotal: number
  taskGemSummaries: Array<TaskGemSummary>

  constructor(epicName: string, epicGemTotal: number, taskGemSummaries: Array<TaskGemSummary>){
    this.epicName = epicName
    this.epicGemTotal = epicGemTotal
    this.taskGemSummaries = taskGemSummaries
  }
}

export class TaskGemSummary {
  taskKey: string
  taskSummary: string
  parentSummary: string
  gemsEarned: number
  noveltyMultiplier: number
  inertiaMultiplier: number
  importanceMultiplier: number
  prideMultiplier: number
  speedMultiplier: number
  epicMultiplier: number
  storyPoints: number

  constructor(
    taskKey: string,
    taskSummary: string,
    parentSummary: string,
    gemsEarned: number,
    noveltyMultiplier: number,
    inertiaMultiplier: number,
    importanceMultiplier: number,
    prideMultiplier: number,
    speedMultiplier: number,
    epicMultiplier: number,
    storyPoints: number
  ){
    this.taskKey = taskKey
    this.taskSummary = taskSummary
    this.parentSummary = parentSummary
    this.gemsEarned = gemsEarned
    this.noveltyMultiplier = noveltyMultiplier
    this.inertiaMultiplier = inertiaMultiplier
    this.importanceMultiplier = importanceMultiplier
    this.prideMultiplier = prideMultiplier
    this.speedMultiplier = speedMultiplier
    this.epicMultiplier = epicMultiplier
    this.storyPoints = storyPoints
  }
}
