import { TimeOfDay } from "./TimeOfDay";
import dayjs from "dayjs";

export interface AlertConfiguration {
  alarmStartTimeOfDay: TimeOfDay
  alarmEndTimeofDay: TimeOfDay
  shouldCallAndText: boolean
  shouldNotify: boolean
  snoozeUntil: dayjs.Dayjs | null
  prefix: string
}