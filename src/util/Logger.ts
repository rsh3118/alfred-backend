enum LogColor {
  RESET = "\x1b[0m",
  BRIGHT = "\x1b[1m",
  DIM = "\x1b[2m",
  UNDERSCORE = "\x1b[4m",
  BLINK = "\x1b[5m",
  REVERSE = "\x1b[7m",
  HIDDEN = "\x1b[8m",

  FG_BLACK = "\x1b[30m",
  FG_RED = "\x1b[31m",
  FG_GREEN = "\x1b[32m",
  FG_YELLOW = "\x1b[33m",
  FG_BLUE = "\x1b[34m",
  FG_MAGENTA = "\x1b[35m",
  FG_CYAN = "\x1b[36m",
  FG_WHITE = "\x1b[37m",

  BG_BLACK = "\x1b[40m",
  BG_RED = "\x1b[41m",
  BG_GREEN = "\x1b[42m",
  BG_YELLOW = "\x1b[43m",
  BG_BLUE = "\x1b[44m",
  BG_MAGENTA = "\x1b[45m",
  BG_CYAN = "\x1b[46m",
  BG_WHITE = "\x1b[47m"
}

export class Logger {
  constructor(private identifier: string, private enabled: boolean = true){}

  info(msg?: string, obj?: any){
     this.log(LogColor.FG_WHITE, msg, obj)
  }

  error(msg?: string, obj?: any){
    this.log(LogColor.FG_RED, msg, obj)
  }

  private log(color: LogColor, msg?: string, obj?: any){
    if (!this.enabled) {
      return
    }
    if(obj instanceof Map){
      obj = Array.from(obj.entries())
    }
    let detailedLog = 
    obj ? 
    `
${JSON.stringify(obj, null, 2)}
    `
    :
    ""
    console.log(`${color}%s${LogColor.RESET}`, `[${this.identifier}] ${msg} ${detailedLog}`)
  }
}