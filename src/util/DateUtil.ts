import dayjs, { Dayjs } from "dayjs";

import utc from "dayjs/plugin/utc" 
import timezone from "dayjs/plugin/timezone" 
import { DateTime } from "luxon";

dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("America/Los_Angeles")



export function convertToCST(day: dayjs.Dayjs){
    return day.subtract(6, 'hour')
}

export function convertToGMT(day: dayjs.Dayjs){
    return day.add(6, 'hour')
}

export function isSameDay(day1: dayjs.Dayjs, day2: dayjs.Dayjs){
    return day1.format('MM/DD/YYYY') === day2.format('MM/DD/YYYY')
} 

export function isSameWeek(day1: dayjs.Dayjs, day2: dayjs.Dayjs){
    return day1.startOf('week').format('MM/DD/YYYY') === day2.startOf('week').format('MM/DD/YYYY')
}

export function isSameMonth(day1: dayjs.Dayjs, day2: dayjs.Dayjs){
    return day1.format('MM/YYYY') === day2.format('MM/YYYY')
}

export function isEndOfMonth(day1: dayjs.Dayjs){
    // day1 = convertToGMT(day1)
    return day1.format('MM/DD/YYYY') === day1.endOf('month').format('MM/DD/YYYY')
}

export function isWeekday(day1: dayjs.Dayjs){
    return day1.day() < 5
}

export function isWeekend(day1: dayjs.Dayjs){
    return day1.day() > 4
}

export function isFriday(day1: dayjs.Dayjs){
    return day1.day() === 4
}

export function getMinutesLeftInADay(){
    let currentDay = DateTime.now().setZone('America/Los_Angeles')
    let hour = currentDay.hour
    let minute = currentDay.minute
    let minutes_left = 60*(22 - hour) + (60 - minute)
    return Math.max(minutes_left, 0)
    
}

export function convertToDate(dateString: string, format: string): Dayjs {
    return dayjs(dateString, format)
}