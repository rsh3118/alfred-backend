export function replaceEscapeCharacters(str: string){
    /*
    Ampersand	&amp;	&
    Less-than	&lt;	<
    Greater-than	&gt;	>
    Quotes	&quot;	"
    Apostrophe	&apos;	'
    */
    let escapeCharacterMap = new Map<string, string>();
    escapeCharacterMap.set(`&`, `&amp;`)
    escapeCharacterMap.set(`<`, `&lt;`)
    escapeCharacterMap.set(`>`, `&gt;`)
    escapeCharacterMap.set(`"`, `&quot;`)
    escapeCharacterMap.set(`'`, `&apos;`)
    let escapeCharacters = [...escapeCharacterMap.keys()]
    for(let i = 0; i < escapeCharacters.length; i++){
        let char = escapeCharacters[i]
        str = str.split(char).join(escapeCharacterMap.get(char))
    }
    return str
}